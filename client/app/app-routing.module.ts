import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FrontendComponent } from './frontend/frontend.component';
import { BackendComponent } from './backend/backend.component';
import { AdminComponent } from './admin/admin.component';
import { AdminLogin } from './admin/admin-login.component';
import { PageNotFoundComponent } from './page-not-found.component';
import { FRONTEND_ROUTES } from './frontend/routes';
import { BACKEND_ROUTES } from './backend/routes';
import { ADMIN_ROUTES } from './admin/routes';

const ROUTES: Routes = [
    {
        path: '',
        component: FrontendComponent,
        children: FRONTEND_ROUTES
    },
    {
        path: '',
        component: BackendComponent,
        children: BACKEND_ROUTES
    },
    {
        path: '',
        component: AdminComponent,
        children: ADMIN_ROUTES
    },
    {
        path: 'admin/login',
        component: AdminLogin
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(ROUTES)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}
