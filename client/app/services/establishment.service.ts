import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL } from './../config/constants';
import { Observable } from 'rxjs/Observable';
import { Establishment } from './../backend/api-response-types/establishment';
import { EstablishmentsData } from './../backend/api-response-types/establishments-data';
import { SavedEstablishmentData } from './../backend/api-response-types/saved-establishment-data';
import { EstablishmentAdminInfoResponse } from './../types/establishment-admin-info-response';

@Injectable()

export class EstablishmentService {
    constructor(
        private http: HttpClient
    ) {}

    getEstablishment(establishmentId: string): Observable<EstablishmentsData> {
        return this.http.get<EstablishmentsData>(`${API_BASE_URL}/api/establishments/${establishmentId}?filter={"include":"photos"}`);
    }

    saveSearchedEstablishment(establishmentData: SavedEstablishmentData): Observable<SavedEstablishmentData> {
        return this.http.post<SavedEstablishmentData>(`${API_BASE_URL}/api/searches`, establishmentData);
    }

    search(term: string): Observable<Establishment[]> {
        return this.http.get<Establishment[]>(`${API_BASE_URL}/api/establishments?filter={"where":{"name":{"like":"${term}","options":"i"}},"order":"name ASC","limit":10}`);
    }

    getSavedEstablishments(bizAccountId: string): Observable<SavedEstablishmentData[]> {
        return this.http.get<SavedEstablishmentData[]>(`${API_BASE_URL}/api/searches?filter={"where":{"bizaccountId":"${bizAccountId}"}}`);
    }

    getSavedEstablishmentsByIds(csvIds: string): Observable<Establishment[]> {
        return this.http.get<Establishment[]>(`${API_BASE_URL}/api/establishments?filter={"where":{"id":{"inq":[${csvIds}]}},"include":"photos"}`);
    }

    getEstablishmentAdminInfo(establishmentId: string): Observable<EstablishmentAdminInfoResponse> {
        return this.http.get<EstablishmentAdminInfoResponse>(`${API_BASE_URL}/api/establishments/${establishmentId}/account`);
    }

    createEstablishment(establishment: Object): Observable<EstablishmentsData> {
        return this.http.post<EstablishmentsData>(`${API_BASE_URL}/api/establishments`, establishment);
    }

    updateEstablishment(establishmentId: string, establishment: Object): Observable<EstablishmentsData> {
        return this.http.put<EstablishmentsData>(`${API_BASE_URL}/api/establishments/${establishmentId}`, establishment);
    }
}