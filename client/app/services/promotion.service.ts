import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL } from './../config/constants';
import { Observable } from 'rxjs/Observable';
import { PromotionResponseType } from './../backend/api-response-types/promotion-response-type';
import { PromotionType } from './../backend/types/promotion-type';

@Injectable()

export class PromotionService {
    constructor(
        private http: HttpClient
    ) {}

    createPromotion(promotion: PromotionType): Observable<PromotionResponseType> {
        return this.http.post<PromotionResponseType>(`${API_BASE_URL}/api/promotions`, promotion);
    }

    getPromotions(establishmentId: string, sortyBy: string = "revenue"): Observable<PromotionResponseType[]> {
        let sortColumn = (sortyBy === "revenue") ? "amount" : "startsAt";
        return this.http.get<PromotionResponseType[]>(`${API_BASE_URL}/api/promotions?filter={"where":{"establishmentId":"${establishmentId}"},"order":"${sortColumn} DESC"}`);
    }

    getPromotion(id: string): Observable<PromotionResponseType> {
        return this.http.get<PromotionResponseType>(`${API_BASE_URL}/api/promotions/${id}`);
    }
}
