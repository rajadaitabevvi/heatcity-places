import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL } from './../config/constants';
import { Observable } from 'rxjs/Observable';
import { BizAccountResponse } from './../types/biz-account-response';
import 'rxjs/add/operator/map';
import { BizAccount } from './../backend/types/bizAccount';
import { AuthService } from './auth.service';

@Injectable()

export class BizAccountService {
    constructor(
        private http: HttpClient,
        private authService: AuthService
    ) {}

    findUserById(bizAccountId: string): Observable<BizAccountResponse> {
        return this.http.get<BizAccountResponse>(`${API_BASE_URL}/api/bizaccounts/${bizAccountId}?access_token=${this.authService.getAPIAccessToken()}`);
    }

    // Gets business account establishment id from the local storage.
    getEstablishmentId(): string {
        return localStorage.getItem("establishmentId");
    }

    // Gets business account user id from the local storage.
    getBizAccountId(): string {
        let currentUser = JSON.parse(localStorage.getItem("currentUser"));
        return currentUser.userId;
    }

    createBizAccount(bizAccount: Object): Observable<BizAccount> {
        return this.http.post<BizAccount>(`${API_BASE_URL}/api/bizaccounts`, bizAccount);
    }

    updateBizAccount(bizAccountId: string, bizAccount: Object): Observable<BizAccount> {
        return this.http.put<BizAccount>(`${API_BASE_URL}/api/bizaccounts/${bizAccountId}?access_token=${this.authService.getAPIAccessToken()}`, bizAccount);
    }
}