import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL } from './../config/constants';
import { Observable } from 'rxjs/Observable';
import { AccountResponse } from './../types/account-response';
import { AuthService } from './auth.service';

@Injectable()

export class AccountService {
    constructor(
        private http: HttpClient,
        private authService: AuthService
    ) {}

    getById(accountId: string): Observable<AccountResponse> {
        return this.http.get<AccountResponse>(`${API_BASE_URL}/api/accounts/${accountId}?access_token=${this.authService.getAPIAccessToken()}`);
    }

    create(account: Object): Observable<AccountResponse> {
        return this.http.post<AccountResponse>(`${API_BASE_URL}/api/accounts`, account);
    }

    update(accountId: string, account: Object): Observable<AccountResponse> {
        return this.http.put<AccountResponse>(`${API_BASE_URL}/api/accounts/${accountId}?access_token=${this.authService.getAPIAccessToken()}`, account);
    }
}