import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { API_BASE_URL } from './../config/constants';
import { Observable } from 'rxjs/Observable';
import { SigninResponseType } from './../frontend/api-response-types/signin-response-type';
import { AdminLoginResponseType } from './../admin/api-response-types/admin-login-response-type';

@Injectable()

export class AuthService {
    constructor(
        private http: HttpClient
    ) {}

    bizAccountLogin(email: string, password: string): Observable<SigninResponseType> {
        return this.http.post<SigninResponseType>(`${API_BASE_URL}/api/bizaccounts/login`, {"email":email,"password":password});
    }

    bizAccountLogout(): void {
        this.http.post(`${API_BASE_URL}/api/bizaccounts/logout`, {}, {
            headers: new HttpHeaders().set('Authorization', this.getUserAuthToken()),
        })
        .subscribe(
            data => {
                // remove user data from local storage to log user out
                localStorage.removeItem('currentUser');

                // Remove establishmentId item from local storage.
                localStorage.removeItem('establishmentId');
            },
            error => {
                console.log('Failed to logout');
                console.log(error);
            }
        );
    }

    isBizAccountLoggedIn(): boolean {
        return localStorage.getItem("currentUser") ? true : false;
    }

    getUserAuthToken(): string {
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        return currentUser.id;
    }

    adminAccountLogin(username: string, password: string): Observable<AdminLoginResponseType> {
        return this.http.post<AdminLoginResponseType>(`${API_BASE_URL}/api/Users/login`, {"username":username,"password":password});
    }

    adminAccountLogout(): void {
        this.http.post(`${API_BASE_URL}/api/Users/logout`, {}, {
            headers: new HttpHeaders().set('Authorization', this.getAdminAuthToken()),
        })
        .subscribe(
            data => {
                // remove admin user data from local storage to log admin user out
                localStorage.removeItem('adminUser');
            },
            error => {
                console.log('Failed to logout');
                console.log(error);
            }
        );
    }

    isAdminLoggedIn(): boolean {
        return localStorage.getItem("adminUser") ? true : false;
    }

    getAdminAuthToken(): string {
        var adminUser = JSON.parse(localStorage.getItem("adminUser"));
        return adminUser.id;
    }

    getAPIAccessToken(): string {
        return this.isAdminLoggedIn() ? this.getAdminAuthToken() : this.getUserAuthToken();
    }
}