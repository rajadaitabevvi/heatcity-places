import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AmChartsModule } from '@amcharts/amcharts3-angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { FrontendComponent } from './frontend/frontend.component';
import { HomeComponent } from './frontend/home.component';
import { SigninComponent } from './frontend/signin.component';
import { SignupComponent } from './frontend/signup.component';

import { BackendComponent } from './backend/backend.component';
import { DashboardComponent } from './backend/dashboard.component';
import { AnalyticsVisitorsComponent } from './backend/analytics-visitors.component';
import { AnalyticsTimeComponent } from './backend/analytics-time.component';
import { AnalyticsDemographicComponent } from './backend/analytics-demographic.component';
import { CompareEstablishmentsComponent } from './backend/compare-establishments.component';
import { SearchEstablishmentComponent } from './backend/search-establishment.component';
import { EstablishmentDetailComponent } from './backend/establishment-detail.component';
import { SavedEstablishmentsComponent } from './backend/saved-establishments.component';
import { CreatePromotionComponent } from './backend/create-promotion.component';
import { PromotionsComponent } from './backend/promotions.component';
import { PromotionsCalendarComponent } from './backend/promotions-calendar.component';
import { BusinessAccountComponent } from './backend/business-account.component';
import { UpdateBusinessAccountComponent } from './backend/update-business-account.component';

import { AdminLogin } from './admin/admin-login.component';
import { AdminComponent } from './admin/admin.component';
import { AdminDashboardComponent } from './admin/admin-dashboard.component';
import { AdminEstablishmentComponent } from './admin/admin-establishment.component';
import { AdminSearchEstablishmentComponent } from './admin/admin-search-establishment.component';
import { AdminCreateEstablishmentComponent } from './admin/admin-create-establishment-component';
import { AdminUpdateEstablishmentComponent } from './admin/admin-update-establishment-component';

import { PageNotFoundComponent } from './page-not-found.component';

import { AuthService } from './services/auth.service';
import { UserAuthGuard } from './guards/user-auth.guard';
import { AdminAuthGuard } from './guards/admin-auth.guard';
import { EstablishmentService } from './services/establishment.service';
import { PromotionService } from './services/promotion.service';
import { BizAccountService } from './services/biz-account.service';
import { AccountService } from './services/account.service';

@NgModule({
  declarations: [
    AppComponent,
    FrontendComponent,
    HomeComponent,
    SigninComponent,
    SignupComponent,
    BackendComponent,
    DashboardComponent,
    AnalyticsVisitorsComponent,
    AnalyticsTimeComponent,
    AnalyticsDemographicComponent,
    CompareEstablishmentsComponent,
    SearchEstablishmentComponent,
    EstablishmentDetailComponent,
    SavedEstablishmentsComponent,
    CreatePromotionComponent,
    PromotionsComponent,
    PromotionsCalendarComponent,
    BusinessAccountComponent,
    UpdateBusinessAccountComponent,
    AdminLogin,
    AdminComponent,
    AdminDashboardComponent,
    AdminEstablishmentComponent,
    AdminSearchEstablishmentComponent,
    AdminCreateEstablishmentComponent,
    AdminUpdateEstablishmentComponent,
    PageNotFoundComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    AmChartsModule
  ],

  providers: [
    AuthService,
    UserAuthGuard,
    AdminAuthGuard,
    EstablishmentService,
    PromotionService,
    BizAccountService,
    AccountService
  ],

  bootstrap: [
    AppComponent
  ]
})

export class AppModule {}
