import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { EstablishmentService } from './../services/establishment.service';
import { BizAccountService } from './../services/biz-account.service';
import { EstablishmentsData, OperatingHours } from './../backend/api-response-types/establishments-data';
import { BizAccount } from './../backend/types/bizAccount';
import { AccountService } from './../services/account.service';
import { Account } from './../types/account';

@Component({
    selector: 'admin-create-establishment',
    templateUrl: './admin-create-establishment.component.html'
})

export class AdminCreateEstablishmentComponent {
    activeTab: string;

    establishment: EstablishmentsData;

    bizAccount: BizAccount;

    account: Account;

    notificationMessage: string;

    submitted: boolean;

    establishmentCreated: boolean;

    timeOptions: string[];

    createdEstablishmentId: string;

    createdBizAccountId: string;

    constructor(
        private establishmentService: EstablishmentService,
        private bizAccountService: BizAccountService,
        private accountService: AccountService,
        private router: Router
    ) {
        this.activeTab = 'general-details';
        this.establishment = new EstablishmentsData();
        this.bizAccount = new BizAccount();
        this.account = new Account();
        this.notificationMessage = "";
        this.submitted = false;
        this.establishmentCreated = false;
        this.timeOptions = [
            "12:00 am",
            "12:30 am",
            "01:00 am",
            "01:30 am",
            "02:00 am",
            "02:30 am",
            "03:00 am",
            "03:30 am",
            "04:00 am",
            "04:30 am",
            "05:00 am",
            "05:30 am",
            "06:00 am",
            "06:30 am",
            "07:00 am",
            "07:30 am",
            "08:00 am",
            "08:30 am",
            "09:00 am",
            "09:30 am",
            "10:00 am",
            "10:30 am",
            "11:00 am",
            "11:30 am",
            "12:00 pm",
            "12:30 pm",
            "01:00 pm",
            "01:30 pm",
            "02:00 pm",
            "02:30 pm",
            "03:00 pm",
            "03:30 pm",
            "04:00 pm",
            "04:30 pm",
            "05:00 pm",
            "05:30 pm",
            "06:00 pm",
            "06:30 pm",
            "07:00 pm",
            "07:30 pm",
            "08:00 pm",
            "08:30 pm",
            "09:00 pm",
            "09:30 pm",
            "10:00 pm",
            "10:30 pm",
            "11:00 pm",
            "11:30 pm"
        ];
        this.createdEstablishmentId = "";
        this.createdBizAccountId = "";
    }

    setTab(tab: string): void {
        this.activeTab = tab;
    }

    createEstablishment(): void {
        this.submitted = true;

        // Get submitted establishment data.
        let establishmentData = {
            "name": this.establishment.name,
            "address": this.establishment.address,
            "url": this.establishment.url,
            "capacity": this.establishment.capacity,
            "pricing": this.establishment.pricing,
            "description": this.establishment.description,
            "hcCategory": this.establishment.hcCategory,
            "hcCategories": this.establishment.hcCategoriesCSV.split(','),
            "hcRating": this.establishment.hcRating,
            "operatesAt": this.establishment.operatesAt,
            // Set establishment status.
            "hcActive": this.establishment.hcActive,
            "locationId": "0"
        };

        // First create an establishment.
        this.establishmentService
        .createEstablishment(establishmentData)
        .subscribe(
            data => {
                console.log("establishment is created successfully");
                this.establishment.reset();

                // Set created establishment id.
                this.createdEstablishmentId = data.id;

                // Get business account data.
                let bizAccountData = {
                    "email": this.bizAccount.email,
                    "phoneNumber": this.bizAccount.phoneNumber,
                    // Set business account status.
                    "hcActive": this.bizAccount.hcActive,
                    // Set email as the default password for the business account.
                    "password": this.bizAccount.email,
                    // Link establishment id with the Business account.
                    "establishmentIds": [
                        {
                            "establishmentId": this.createdEstablishmentId
                        }
                    ]
                };

                // Create a business account.
                this.bizAccountService
                .createBizAccount(bizAccountData)
                .subscribe(
                    data => {
                        console.log("business account is created successfully");
                        this.bizAccount.reset();

                        // Set created business account id.
                        this.createdBizAccountId = data.id;

                        // Create an admin account.
                        let adminData = {
                            "email": this.account.email,
                            "phoneNumber": this.account.phoneNumber,
                            // Set business account status.
                            "approved": true,
                            // Set email as the default password for the business account.
                            "password": this.account.email
                        };

                        this.accountService
                        .create(adminData)
                        .subscribe(
                            data => {
                                console.log("admin account is created successfully");
                                this.account.reset();

                                let updateData = {
                                    "accountId": data.id,
                                    "bizAccountId": this.createdBizAccountId
                                };

                                // Set "accountId" in the newly created Establishment.
                                this.establishmentService
                                .updateEstablishment(this.createdEstablishmentId, updateData)
                                .subscribe(
                                    data => {
                                        console.log("relationship with admin and business account is setup successfully");
                                        this.establishmentCreated = true;
                                        this.notificationMessage = "Establishment, Business Account & Admin Account created successfully.";
                                    },
                                    (error: HttpErrorResponse) => {
                                        if (error.status === 0) {
                                            // A client-side or network error occurred. Handle it accordingly.
                                            console.log(`A client-side error occurred: ${error.message}`);
                                            this.notificationMessage = "Request for create establishment failed. Please check your connection and try again later.";
                                        } else {
                                            // The backend returned an unsuccessful response code.
                                            console.log(`Backend returned status code ${error.status}, message: ${error.error.error.message}`);
                                            this.notificationMessage = error.error.error.message;
                                        }
                                    }
                                )
                            },
                            (error: HttpErrorResponse) => {
                                if (error.status === 0) {
                                    // A client-side or network error occurred. Handle it accordingly.
                                    console.log(`A client-side error occurred: ${error.message}`);
                                    this.notificationMessage = "Request for create establishment failed. Please check your connection and try again later.";
                                } else {
                                    // The backend returned an unsuccessful response code.
                                    console.log(`Backend returned status code ${error.status}, message: ${error.error.error.message}`);
                                    this.notificationMessage = error.error.error.message;
                                }
                            }
                        );
                    },
                    (error: HttpErrorResponse) => {
                        if (error.status === 0) {
                            // A client-side or network error occurred. Handle it accordingly.
                            console.log(`A client-side error occurred: ${error.message}`);
                            this.notificationMessage = "Request for create establishment failed. Please check your connection and try again later.";
                        } else {
                            // The backend returned an unsuccessful response code.
                            console.log(`Backend returned status code ${error.status}, message: ${error.error.error.message}`);
                            this.notificationMessage = error.error.error.message;
                        }
                    }
                )
            },
            (error: HttpErrorResponse) => {
                if (error.status === 0) {
                    // A client-side or network error occurred. Handle it accordingly.
                    console.log(`A client-side error occurred: ${error.message}`);
                    this.notificationMessage = "Request for create establishment failed. Please check your connection and try again later.";
                } else {
                    // The backend returned an unsuccessful response code.
                    console.log(`Backend returned status code ${error.status}, message: ${error.error.error.message}`);
                    this.notificationMessage = error.error.error.message;
                }
            }
        )
    }

    gotoCreateEstablishment(): void {
        this.submitted = false;
        this.establishmentCreated = false;
        this.establishment.reset();
        this.bizAccount.reset();
        this.activeTab = 'general-details';
        this.notificationMessage = "";
    }

    gotoBrowseEstablishment(): void {
        this.router.navigate(['/admin/dashboard']);
    }
}