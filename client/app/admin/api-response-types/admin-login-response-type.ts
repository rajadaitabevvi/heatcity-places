export interface AdminLoginResponseType {
    id: string;
    ttl: number;
    userId: string;    
}