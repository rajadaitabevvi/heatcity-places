import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';

@Component({
    selector: 'admin',
    templateUrl: './admin.component.html',
})

export class AdminComponent {
    constructor(
        private authService: AuthService,
        private router: Router        
    ) {}

    doLogout(): void {
        this.authService.adminAccountLogout();

        // Navigate user to the Home page.
        this.router.navigate(['/admin/login']);
    }
}
