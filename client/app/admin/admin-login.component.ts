import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';

@Component({
    selector: 'admin-login',
    templateUrl: './admin-login.component.html'
})

export class AdminLogin {
    loginModel: any;

    loginError: string;

    constructor(
        private authService: AuthService,
        private router: Router        
    ) {
        this.loginModel = {};
        this.loginError = "";
    }

    doLogin(): void {
        this.authService
        .adminAccountLogin(this.loginModel.username, this.loginModel.password)
        .subscribe(
            data => {
                console.log("admin login success callback");
                // Clear the login error.
                this.loginError = "";

                // Save user data in the local storage.
                localStorage.setItem('adminUser', JSON.stringify(data));

                // Redirect to the /admin/dashboard page.
                this.router.navigate(['/admin/dashboard']);
            },
            error => {
                console.log("admin login error callback");
                console.log('Failed to login to admin account');
                this.loginError = "Failed to login. Please provide correct username and password.";
                console.log(error);
            }
        )
    }
}