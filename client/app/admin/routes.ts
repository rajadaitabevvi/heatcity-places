import { Routes } from '@angular/router';

import { AdminLogin } from './admin-login.component';
import { AdminDashboardComponent } from './admin-dashboard.component';
import { AdminEstablishmentComponent } from './admin-establishment.component';
import { AdminCreateEstablishmentComponent } from './admin-create-establishment-component';
import { AdminUpdateEstablishmentComponent } from './admin-update-establishment-component';

import { AdminAuthGuard } from './../guards/admin-auth.guard';

export const ADMIN_ROUTES: Routes = [
    {
        path: 'admin/dashboard',
        component: AdminDashboardComponent,
        canActivate: [AdminAuthGuard]
    },
    {
        path: 'admin/establishment/create',
        component: AdminCreateEstablishmentComponent,
        canActivate: [AdminAuthGuard]
    },
    {
        path: 'admin/establishment/update/:id',
        component: AdminUpdateEstablishmentComponent,
        canActivate: [AdminAuthGuard]
    },
    {
        path: 'admin/establishment/:id',
        component: AdminEstablishmentComponent,
        canActivate: [AdminAuthGuard]
    }
];