import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL, MONTHS } from './../config/constants';
import { AnalyticsDemographicResponse, AgeRangeData } from './api-response-types/analytics-demographic-response';
import { AnalyticsPieChartData, GenderStats } from './api-response-types/analytics-pie-chart-data';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';
import { DateRange } from './types/date-range';
import { AgeRangePieChartData } from './types/age-range-pie-chart-data';
import { BizAccountService } from './../services/biz-account.service';

@Component({
    selector: 'analytics-demographic',
    templateUrl: './analytics-demographic.component.html'
})

export class AnalyticsDemographicComponent implements OnInit {
    establishmentId: string;

    today: string;

    thisWeek: string;

    thisMonth: string;

    thisYear: string;

    todayAgeRange: string;

    todayGender: string;

    thisWeekAgeRange: string;

    thisWeekGender: string;

    thisMonthAgeRange: string;

    thisMonthGender: string;

    thisYearAgeRange: string;

    thisYearGender: string;

    amChart: AmChart;

    dateRange: DateRange;

    // Notification text related to graph data (loading/error handling etc.).
    graphNotifications: string;

    ageRangePieChartNotifications: string;

    genderPieChartNotifications: string;

    constructor(
        private http: HttpClient,
        private amChartsService: AmChartsService,
        private bizAccountService: BizAccountService
    ) {
        this.establishmentId = this.bizAccountService.getEstablishmentId();

        this.setTimingStrings();

        this.todayAgeRange = '';
        this.todayGender = '';

        this.thisWeekAgeRange = '';
        this.thisWeekGender = '';

        this.thisMonthAgeRange = '';
        this.thisMonthGender = '';

        this.thisYearAgeRange = '';
        this.thisYearGender = '';

        this.dateRange = new DateRange();

        this.graphNotifications = 'Submit start date and end date to check the demographic pie charts.';

        this.ageRangePieChartNotifications = '';

        this.genderPieChartNotifications = '';
    }

    setTimingStrings(): void {
        let currentDate = new Date();
        let currentYear = new Date().getFullYear();
        let currentMonth = new Date().getMonth();

        // Set "today"
        // weekday along with long date: Wednesday, October 4, 2017
        let options = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        this.today = currentDate.toLocaleString("en-US", options);

        // Set "thisWeek"
        // Sunday - Monday
        this.thisWeek = "Sunday - Monday";

        // Set "thisMonth"
        // October 2017
        this.thisMonth = `${MONTHS[currentMonth]} ${currentYear}`;

        // Set "thisYear"
        // Jan 2017 - Today
        this.thisYear = `${MONTHS[0]} ${currentYear} - Today`;
    }

    ngOnInit(): void {
        this.loadDemographicStats();
    }

    loadDemographicStats(): void {
        console.log("loadDemographicStats API Call");
        console.log(`${API_BASE_URL}/api/establishments/${this.establishmentId}/demographicStats`);

        this.http
            .get<AnalyticsDemographicResponse>(`${API_BASE_URL}/api/establishments/${this.establishmentId}/demographicStats`)
            .subscribe(
                // Success callback.
                data => {
                    console.log('Success call back');
                    console.log('data');
                    console.log(data);
                    // Set Demographic statistics.
                    this.todayAgeRange = (data.today && data.today.ages) ? this.getAgeRangeWithMaxCount(data.today.ages) : 'Age Range Not Available';
                    this.todayGender = (data.today && data.today.gender) ? this.getDemographicGender(data.today.gender) : 'Gender Not Available';

                    this.thisWeekAgeRange = (data.week && data.week.ages) ? this.getAgeRangeWithMaxCount(data.week.ages) : 'Age Range Not Available';
                    this.thisWeekGender = (data.week && data.week.gender) ? this.getDemographicGender(data.week.gender) : 'Gender Not Available';

                    this.thisMonthAgeRange = (data.month && data.month.ages) ? this.getAgeRangeWithMaxCount(data.month.ages) : 'Age Range Not Available';
                    this.thisMonthGender = (data.month && data.month.gender) ? this.getDemographicGender(data.month.gender) : 'Gender Not Available';

                    this.thisYearAgeRange = (data.year && data.year.ages) ? this.getAgeRangeWithMaxCount(data.year.ages) : 'Age Range Not Available';
                    this.thisYearGender = (data.year && data.year.gender) ? this.getDemographicGender(data.year.gender) : 'Gender Not Available';
                },

                // Error callback.
                err => {
                    console.log("Something went wrong!");
                    console.log("Got error in API response");
                    console.log("Error details");
                    console.log(err);
                }
            );
    }

    // Gets AgeRangeData object with maximum count.
    getAgeRangeWithMaxCount(ages: AgeRangeData[]): string {
        let maxIndex = 0;
        let maxCount = 0;

        for (let i = 0; i < ages.length; i++) {
            if (ages[i].ageCount > maxCount) {
                maxCount = ages[i].ageCount;
                maxIndex = i;
            }
        }

        // Check if there are 0 counts in all then return a message.
        return (maxCount > 0) ? ages[maxIndex].age : 'Age Range Not Available';
    }

    getDemographicGender(genderData): string {
        let gender = 'Gender Not Available';

        // If genderData is defined
        if (genderData !== undefined) {
            // If both male and female are defined
            if ((genderData.male !== undefined) && (genderData.female !== undefined)) {
                // If both are unequal then select one with max value.
                if (genderData.male !== genderData.female) {
                    gender = (genderData.male > genderData.female) ? 'Male' : 'Female';
                }
            }
            // Else if male is defined
            else if (genderData.male !== undefined) {
                gender = (genderData.male > 0) ? 'Male' : '';
            }
            // Else if female is defined
            else if (genderData.female !== undefined) {
                gender = (genderData.female > 0) ? 'Female' : '';
            }
        }

        return gender;
    }

    ngOnDestroy(): void {
        if (this.amChart) {
            this.amChartsService.destroyChart(this.amChart);
        }
    }

    renderGraph(): void {
        console.log('renderGraph api call');
        console.log(`${API_BASE_URL}/api/establishments/${this.establishmentId}/demographicsGraphs?startDate=${this.dateRange.startDate}&endDate=${this.dateRange.endDate}`);

        if (this.amChart) {
            this.amChartsService.destroyChart(this.amChart);
        }
        this.graphNotifications = 'Loading...';

        // Get graph data from an API.
        this.http
            .get<AnalyticsPieChartData>(`${API_BASE_URL}/api/establishments/${this.establishmentId}/demographicsGraphs?startDate=${this.dateRange.startDate}&endDate=${this.dateRange.endDate}`)
            .subscribe(
                // Success callback.
                data => {
                    console.log('success call back');
                    console.log('data');
                    console.log(data);
                    console.log('data.total.ages');
                    console.log(data.total.ages);
                    this.graphNotifications = '';

                    // Age range pie chart.
                    let ageRangeData: AgeRangeData[];
                    ageRangeData = data.total.ages;

                    if (this.checkIfAgeRangePieChartDataOk(ageRangeData)) {
                        this.ageRangePieChartNotifications = '';

                        let ageRangePieChartData: AgeRangePieChartData[] = [];
                        for (var i = 0; i < ageRangeData.length; i++) {
                            let ageRangeDataObj = new AgeRangePieChartData();
                            ageRangeDataObj.age = ageRangeData[i].age;
                            ageRangeDataObj.ageCount = ageRangeData[i].ageCount;
                            ageRangePieChartData[i] = ageRangeDataObj;
                        }

                        // Render pie chart data.
                        this.amChart = this.amChartsService.makeChart("age-range-pie-chart",
                        {
                            "type": "pie",
                            "theme": "light",
                            "dataProvider": ageRangePieChartData,

                            /*
                            "dataProvider": [
                                {
                                    "age": "13-17",
                                    "ageCount": 5
                                },
                                {
                                    "age": "18-24",
                                    "ageCount": 5
                                },
                                {
                                    "age": "25-34",
                                    "ageCount": 5
                                },
                                {
                                    "age": "35-44",
                                    "ageCount": 5
                                },
                                {
                                    "age": "45-54",
                                    "ageCount": 5
                                },
                                {
                                    "age": "55-64",
                                    "ageCount": 5
                                },
                                {
                                    "age": "65+",
                                    "ageCount": 5
                                },
                            ],
                            */

                            "titleField": "age",
                            "valueField": "ageCount",
                            "labelRadius": 5,
                            "radius": "40%",
                            "innerRadius": "60%",
                            "labelText": "[[age]]",

                            "allLabels": [
                                {
                                    "align": "center",
                                    "bold": true,
                                    "color": "#648296",
                                    "id": "Age Range",
                                    "size": 14,
                                    "tabIndex": 0,
                                    "text": "Age Range",
                                    "x": "0%",
                                    "y": "48%"
                                }
                            ],

                            "export": {
                                "enabled": false
                            }
                        });
                    } else {
                        this.ageRangePieChartNotifications = 'No age range data available to render pie chart.';
                    }

                    // Gender pie chart.
                    let genderData: GenderStats;
                    genderData = data.total.gender;

                    if (this.checkIfGenderPieChartDataOk(genderData)) {
                        this.genderPieChartNotifications = '';

                        // Render pie chart data.
                        this.amChart = this.amChartsService.makeChart("gender-pie-chart",
                        {
                            "type": "pie",
                            "theme": "light",
                            "dataProvider": [
                                {
                                    "gender": "Male",
                                    "count": genderData.male
                                },
                                {
                                    "gender": "Female",
                                    "count": genderData.female
                                }
                            ],
                            "titleField": "gender",
                            "valueField": "count",
                            "labelRadius": 5,
                            "radius": "40%",
                            "innerRadius": "60%",
                            "labelText": "[[gender]]",

                            "allLabels": [
                                {
                                    "align": "center",
                                    "bold": true,
                                    "color": "#648296",
                                    "id": "Gender",
                                    "size": 14,
                                    "tabIndex": 0,
                                    "text": "Gender",
                                    "x": "0%",
                                    "y": "48%"
                                }
                            ],

                            "export": {
                                "enabled": false
                            }
                        });
                    } else {
                        this.genderPieChartNotifications = 'No gender data available to render pie chart.';
                    }
                },

                // Error callback.
                err => {
                    this.graphNotifications = 'Problem loading the pie charts data. Please try again after some time.';
                    console.log("Something went wrong!");
                    console.log("Got error in API response");
                    console.log("Error details");
                    console.log(err);
                }
            );
    }

    checkIfAgeRangePieChartDataOk(ageRangeData: AgeRangeData[]): boolean {
        if (ageRangeData.length ===  0) {
            return false;
        }

        let ageCount = 0;
        for (var i = 0; i < ageRangeData.length; i++) {
            ageCount += ageRangeData[i].ageCount;
            if (ageCount > 0) {
                break;
            }
        }

        return (ageCount > 0) ? true : false;
    }

    checkIfGenderPieChartDataOk(genderData: GenderStats): boolean {
        return ((genderData.male == 0) && (genderData.female == 0)) ? false : true;
    }
}
