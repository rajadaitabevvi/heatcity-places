import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { API_BASE_URL } from './../config/constants';
import { PromotionType } from './types/promotion-type';
import { PromotionService } from './../services/promotion.service';
import { BizAccountService } from './../services/biz-account.service';

@Component({
    selector: 'create-promotion',
    templateUrl: './create-promotion.component.html'
})

export class CreatePromotionComponent implements OnInit {
    establishmentId: string;
    promotion: PromotionType;
    cityOptions: string[];
    radiusOptions: number[];
    ageRangeOptions: string[];
    step: string;
    promoCreated: boolean;
    geographicalRangeValidationFailed: boolean;

    constructor(
        private promotionService: PromotionService,
        private bizAccountService: BizAccountService,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {
        this.establishmentId = this.bizAccountService.getEstablishmentId();

        this.cityOptions = [
            'New York'
        ];

        this.radiusOptions = [
            0.5,
            1,
            5
        ];

        this.ageRangeOptions = [
            '13-17',
            '18-24',
            '25-34',
            '35-44',
            '45-54',
            '55-64',
            '65+'
        ];

        this.step = 'submit';
        this.promoCreated = false;

        this.geographicalRangeValidationFailed = false;
    }

    ngOnInit(): void {
        // Create a new Promotion instance.
        this.promotion = new PromotionType();

        this.activatedRoute.params.subscribe((params: Params) => {
            if (params['id'] !== undefined) {
                // Get existing promotions data and copy it to create a new repeat promotion.
                this.promotionService
                .getPromotion(params['id'])
                .subscribe(
                    data => {
                        // Copy promotions data got from API.
                        this.promotion.name = data.name;
                        this.promotion.description = data.description;
                        this.promotion.amount = data.amount;
                        this.promotion.startsAt = data.startsAt;
                        this.promotion.endsAt = data.endsAt;
                        this.promotion.areaName = data.areaName;
                        this.promotion.promoRadius = data.promoRadius;
                        this.promotion.ageRange = data.ageRange;
                        this.promotion.targeted = data.targeted;
                        this.promotion.establishmentId = data.establishmentId;
                        this.promotion.startDate = '';
                        this.promotion.startTime = '';
                        this.promotion.endDate = '';
                        this.promotion.endTime = '';
                    },
                    error => {
                        console.log('Error while getting promotion data');
                        console.log(error);
                    }
                )
            }
        });
    }

    submitPromo(): void {
        if ((this.promotion.areaName == "") && (this.promotion.promoRadius == 0)) {
            this.geographicalRangeValidationFailed = true;
        } else {
            this.geographicalRangeValidationFailed = false;

            // Set establishment id.
            this.promotion.establishmentId = this.establishmentId;
    
            // Set name as same as description.
            this.promotion.name = this.promotion.description;
    
            // Set start date time values.
            let times: string[] = this.promotion.startTime.split(":");
            let hours: number = Number(times[0]);
            let mins: number = Number(times[1]);
            let secs: number = 0;
            let millisecs: number = 0;
    
            let dateTimeVal: Date = new Date(this.promotion.startDate);
            dateTimeVal.setHours(hours, mins, secs, millisecs);
            this.promotion.startsAt = dateTimeVal.toISOString();
    
            // Set end date time values.
            times = this.promotion.endTime.split(":");
            hours = Number(times[0]);
            mins = Number(times[1]);
    
            dateTimeVal = new Date(this.promotion.endDate);
            dateTimeVal.setHours(hours, mins, secs, millisecs);
            this.promotion.endsAt = dateTimeVal.toISOString();

            this.step = 'preview';
        }
    }

    createPromo(): void {
        this.promotionService
        .createPromotion(this.promotion)
        .subscribe(
            data => {
                this.step = 'completed';
                this.promoCreated = true;
            },
            error => {
                this.step = 'completed';
                this.promoCreated = false;
                console.log('Error while scheduling a new promotion');
                console.log(error);
            }
        );
    }

    cancelPromo(): void {
        this.step = 'submit';
        this.promotion.reset();
    }

    gotoCreatePromo(): void {
        this.step = 'submit';
        this.promoCreated = false;
        this.promotion.reset();
    }

    gotoCheckAllPromos(): void {
        this.router.navigate(['/account/promotions/all']);
    }
}