import { Component, OnInit } from '@angular/core';
import { API_BASE_URL, MONTHS } from './../config/constants';
import { HttpClient } from '@angular/common/http';
import { AnalyticsResponse } from './api-response-types/analytics-response';
import { parseMillisecsIntoHoursMinsSecs } from '../lib/helpers';
import { parseMillisecsIntoMins } from '../lib/helpers';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';
import { DateRange } from './types/date-range';
import { AnalyticsChartData, DateWiseVisits, DateWiseTimes } from './api-response-types/analytics-chart-data';
import { BizAccountService } from './../services/biz-account.service';

@Component({
    selector: 'analytics-time',
    templateUrl: './analytics-time.component.html'
})

export class AnalyticsTimeComponent implements OnInit {
    establishmentId: string;

    today: string;

    thisWeek: string;

    thisMonth: string;

    thisYear: string;

    todayTimeSpent: string;
    
    thisWeekTimeSpent: string;

    thisMonthTimeSpent: string;

    thisYearTimeSpent: string;

    amChart: AmChart;
    
    dateRange: DateRange;

    // Notification text related to graph data (loading/error handling etc.).
    graphNotifications: string;

    constructor(
        private http: HttpClient,
        private amChartsService: AmChartsService,
        private bizAccountService: BizAccountService
    ) {
        this.establishmentId = this.bizAccountService.getEstablishmentId();
        //this.establishmentId = '58ccc0f798be8374eb6584bb';

        this.setTimingStrings();

        this.todayTimeSpent = '';
        this.thisWeekTimeSpent = '';
        this.thisMonthTimeSpent = '';
        this.thisYearTimeSpent = '';

        this.dateRange = new DateRange();

        this.graphNotifications = 'Submit start date and end date to check the time spent graph.';
    }

    setTimingStrings(): void {
        let currentDate = new Date();
        let currentYear = new Date().getFullYear();
        let currentMonth = new Date().getMonth();

        // Set "today"
        // weekday along with long date: Wednesday, October 4, 2017
        let options = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        this.today = currentDate.toLocaleString("en-US", options);

        // Set "thisWeek"
        // Sunday - Monday
        this.thisWeek = "Sunday - Monday";

        // Set "thisMonth"
        // October 2017
        this.thisMonth = `${MONTHS[currentMonth]} ${currentYear}`;

        // Set "thisYear"
        // Jan 2017 - Today
        this.thisYear = `${MONTHS[0]} ${currentYear} - Today`;
    }

    ngOnInit(): void {
        this.loadTimeSpentStats();
    }

    loadTimeSpentStats(): void {
        console.log('Calling API');
        console.log(`${API_BASE_URL}/api/establishments/${this.establishmentId}/statistics`);

        this.http
            .get<AnalyticsResponse>(`${API_BASE_URL}/api/establishments/${this.establishmentId}/statistics`)
            .subscribe(
                // Success callback.
                data => {
                    console.log('loadTimeSpentStats success callback');
                    console.log('data');
                    console.log(data);
                    console.log('data.timeSpent');
                    console.log(data.timeSpent);
                    // Set Time Spent statistics.
                    this.todayTimeSpent = (data.timeSpent && data.timeSpent.today) ? this.formatAvgTimeSpent(data.timeSpent.today) : 'Not Available';
                    this.thisWeekTimeSpent = (data.timeSpent && data.timeSpent.thisWeek) ? this.formatAvgTimeSpent(data.timeSpent.thisWeek) : 'Not Available';
                    this.thisMonthTimeSpent = (data.timeSpent && data.timeSpent.thisMonth) ? this.formatAvgTimeSpent(data.timeSpent.thisMonth) : 'Not Available';
                    this.thisYearTimeSpent = (data.timeSpent && data.timeSpent.thisYear) ? this.formatAvgTimeSpent(data.timeSpent.thisYear) : 'Not Available';
                },

                // Error callback.
                err => {
                    console.log("Something went wrong!");
                    console.log("Got error in API response");
                    console.log("Error details");
                    console.log(err);
                }
            );
    }

    // Formats average time spent data into human readable format.
    // x hr y min
    formatAvgTimeSpent(milliseconds): string {
        let timeData = parseMillisecsIntoHoursMinsSecs(milliseconds);
        return `${timeData[0]} hr ${timeData[1]} min`;
    }

    ngOnDestroy(): void {
        if (this.amChart) {
            this.amChartsService.destroyChart(this.amChart);
        }
    }

    renderGraph(): void {
        if (this.amChart) {
            this.amChartsService.destroyChart(this.amChart);
        }
        this.graphNotifications = 'Loading...';

        console.log('Calling API');
        console.log(`${API_BASE_URL}/api/establishments/${this.establishmentId}/graphStats?startDate=${this.dateRange.startDate}&endDate=${this.dateRange.endDate}`);

        // Get graph data from an API.
        this.http
            .get<AnalyticsChartData>(`${API_BASE_URL}/api/establishments/${this.establishmentId}/graphStats?startDate=${this.dateRange.startDate}&endDate=${this.dateRange.endDate}`)
            .subscribe(
                // Success callback.
                data => {
                    console.log("renderGraph success callback");
                    console.log("data");
                    console.log(data);
                    console.log("data.timeSpent");
                    console.log(data.timeSpent);
                    let timeSpentGraphData: DateWiseTimes[];
                    timeSpentGraphData = data.timeSpent;

                    if (timeSpentGraphData.length > 0) {
                        this.graphNotifications = '';

                        for (var i = 0; i < timeSpentGraphData.length; i++) {
                            timeSpentGraphData[i].date = this.formatDate(timeSpentGraphData[i].date);
                            timeSpentGraphData[i].time = parseMillisecsIntoMins(timeSpentGraphData[i].time);
                        }

                        // Render graph data.
                        this.amChart = this.amChartsService.makeChart("chartdiv",
                        {
                            "type": "serial",
                            "theme": "light",
                            "marginRight": 30,
                            "legend": {
                                "equalWidths": false,
                                "periodValueText": "total: [[value.sum]] (mins)",
                                "position": "top",
                                "valueAlign": "left",
                                "valueWidth": 100
                            },
                            "dataProvider": timeSpentGraphData,
                            "valueAxes": [{
                                "stackType": "regular",
                                "gridAlpha": 0.07,
                                "position": "left",
                                "title": "Time Spent (mins)"
                            }],
                            "graphs": [{
                                "balloonText": "<span style='font-size:14px; color:#000000;'><b>[[value]] (mins)</b></span>",
                                "fillAlphas": 0.6,
                                "lineAlpha": 0.4,
                                "title": "Time Spent (Mins)",
                                "valueField": "time",
                                "type": "smoothedLine"
                            }],
                            "plotAreaBorderAlpha": 0,
                            "marginTop": 10,
                            "marginLeft": 0,
                            "marginBottom": 0,
                            "chartCursor": {
                                "cursorAlpha": 0
                            },
                            "categoryField": "date",
                            "categoryAxis": {
                                "startOnAxis": true,
                                "axisColor": "#DADADA",
                                "gridAlpha": 0.07,
                                "title": "Timeline"
                            },
                            "export": {
                                "enabled": false
                            }
                        });
                    } else {
                        this.graphNotifications = 'No data found to render the graph.';
                    }
                },

                // Error callback.
                err => {
                    this.graphNotifications = 'Problem loading the graph data. Please try again after some time.';
                    console.log("Something went wrong!");
                    console.log("Got error in API response");
                    console.log("Error details");
                    console.log(err);
                }
            );
    }

    formatDate(dateValue: string): string {
        let dateObject = new Date(dateValue);
        let options = {
            day: 'numeric',
            month: 'short'
        };
        return dateObject.toLocaleString("en-US", options);
    }
}
