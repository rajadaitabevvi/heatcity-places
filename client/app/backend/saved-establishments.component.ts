import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL, IMG_BASE_URL } from './../config/constants';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';
import { EstablishmentService } from './../services/establishment.service';
import { SavedEstablishmentData } from './api-response-types/saved-establishment-data';
import { Establishment } from './api-response-types/establishment';
import { EstablishmentCompareData, EstablishmentCompareDataArray } from './types/establishment-compare-data';
import { EstablishmentVisitsCount } from './api-response-types/establishment-visits-count';
import { BizAccountService } from './../services/biz-account.service';

@Component({
    selector: 'saved-establishments',
    templateUrl: './saved-establishments.component.html'
})

export class SavedEstablishmentsComponent implements OnInit {
    establishmentId: string;
    bizAccountId: string;
    savedEstablishmentsData: SavedEstablishmentData[];
    userEstablishment: Establishment;
    notificationMessage: string;
    savedEstablishments: Establishment[];
    establishmentCompareDataArray: EstablishmentCompareDataArray;
    imgBaseUrl: string;

    constructor(
        private http: HttpClient,
        private amChartsService: AmChartsService,
        private establishmentService: EstablishmentService,
        private bizAccountService: BizAccountService
    ) {
        this.establishmentId = this.bizAccountService.getEstablishmentId();
        this.bizAccountId = this.bizAccountService.getBizAccountId();
        this.savedEstablishmentsData = [];
        this.savedEstablishments = [];
        this.notificationMessage = 'Loading...';
        this.establishmentCompareDataArray = {};
        this.imgBaseUrl = IMG_BASE_URL;
    }

    ngOnInit(): void {
        this.loadEstablishmentsData();
    }

    ngOnDestroy(): void {
        this.savedEstablishments.forEach(element => {
            if (this.establishmentCompareDataArray[element.id].amChart) {
                this.amChartsService.destroyChart(this.establishmentCompareDataArray[element.id].amChart);
            }
        });
    }

    loadEstablishmentsData(): void {
        // Load user establishment.
        this.establishmentService
            .getEstablishment(this.establishmentId)
            .subscribe(
                data => {
                    this.userEstablishment = data;
                    // Load saved establishments.
                    this.loadSavedEstablishments();
                },
                error => {
                    console.log('Error while getting user\'s establishments data');
                    console.log(error);
                }
            );
    }

    loadSavedEstablishments(): void {
        this.establishmentService
            .getSavedEstablishments(this.bizAccountId)
            .subscribe(
                data => {
                    this.savedEstablishmentsData = data;
                    let establishmentIds: string[] = [];
                    this.savedEstablishmentsData.forEach(element => {
                        establishmentIds.push(`"${element.establishmentId}"`);
                    });
                    let establishmentCsvIds: string = establishmentIds.join();
                    this.notificationMessage = (establishmentCsvIds === '') ? 'No saved establishments found.' : '';

                    if (establishmentCsvIds !== '') {
                        this.establishmentService
                            .getSavedEstablishmentsByIds(establishmentCsvIds)
                            .subscribe(
                                data => {
                                    console.log("saved establishments");
                                    console.log(data);
                                    this.savedEstablishments = data;
                                    this.setEstablishmentCompareData();
                                },
                                error => {
                                    console.log('Error while getting saved establishments data');
                                    console.log(error);
                                }
                            );
                    }
                },
                error => {
                    console.log('Error while getting saved establishments data');
                    console.log(error);
                }
            );
    }

    setEstablishmentCompareData(): void {
        this.savedEstablishments.forEach(element => {
            // Set establishment compare data.
            let establishmentCompareData: EstablishmentCompareData = new EstablishmentCompareData();
            establishmentCompareData.competitorEstablishmentId = element.id;
            establishmentCompareData.competitorEstablishmentName = element.name;
            establishmentCompareData.competitorEstablishmentVisitsCount = 0;
            establishmentCompareData.ownerEstablishmentId = this.establishmentId;
            establishmentCompareData.ownerEstablishmentName = this.userEstablishment.name;
            establishmentCompareData.ownerEstablishmentVisitsCount = 0;
            establishmentCompareData.graphNotifications = 'Submit start date and end date to check the visits graph.';
            establishmentCompareData.graphReady = false;
            establishmentCompareData.photos = element.photos;

            // Save this establishment compare data into an array.
            this.establishmentCompareDataArray[element.id] = establishmentCompareData;
        });
    }

    renderGraph(chartId: string, startDate: string, endDate: string, savedEstablishmentId: string): void {
        if (this.establishmentCompareDataArray[savedEstablishmentId].amChart) {
            this.amChartsService.destroyChart(this.establishmentCompareDataArray[savedEstablishmentId].amChart);
        }
        this.establishmentCompareDataArray[savedEstablishmentId].graphNotifications = 'Loading...';

        // Get user establishment visits count.
        this.http
            .get<EstablishmentVisitsCount>(`${API_BASE_URL}/api/establishments/${this.establishmentId}/visits?startDate=${startDate}&endDate=${endDate}`)
            .subscribe(
                // Success callback.
                data => {
                    this.establishmentCompareDataArray[savedEstablishmentId].ownerEstablishmentVisitsCount = data.count;

                    // Get selected establishment visits count.
                    this.http
                        .get<EstablishmentVisitsCount>(`${API_BASE_URL}/api/establishments/${savedEstablishmentId}/visits?startDate=${startDate}&endDate=${endDate}`)
                        .subscribe(
                            // Success callback.
                            data => {
                                this.establishmentCompareDataArray[savedEstablishmentId].competitorEstablishmentVisitsCount = data.count;

                                this.establishmentCompareDataArray[savedEstablishmentId].graphNotifications = '';
                                this.establishmentCompareDataArray[savedEstablishmentId].graphReady = true;

                                // Calculate comparison statistics.
                                this.establishmentCompareDataArray[savedEstablishmentId].comparisonStats = this.calculateCompareStats(this.establishmentCompareDataArray[savedEstablishmentId].ownerEstablishmentName, this.establishmentCompareDataArray[savedEstablishmentId].ownerEstablishmentVisitsCount, this.establishmentCompareDataArray[savedEstablishmentId].competitorEstablishmentName, this.establishmentCompareDataArray[savedEstablishmentId].competitorEstablishmentVisitsCount);

                                // Render graph data.
                                this.establishmentCompareDataArray[savedEstablishmentId].amChart = this.amChartsService.makeChart(chartId,
                                {
                                    "type": "serial",
                                    "categoryField": "category",
                                    "rotate": true,
                                    "startDuration": 1,
                                    "colors": [
                                      "#cd3535",
                                      "#658396",
                                      "#B0DE09",
                                      "#0D8ECF",
                                      "#2A0CD0",
                                      "#CD0D74",
                                      "#CC0000",
                                      "#00CC00",
                                      "#0000CC",
                                      "#DDDDDD",
                                      "#999999",
                                      "#333333",
                                      "#990000"
                                    ],
                                    "categoryAxis": {
                                      "gridPosition": "start",
                                      "autoGridCount": false,
                                      "axisThickness": 0,
                                      "gridCount": 0,
                                      "gridThickness": 0,
                                      "minVerticalGap": 30,
                                      "showFirstLabel": false,
                                      "showLastLabel": false,
                                      "tickLength": 0
                                    },
                                    "trendLines": [],
                                    "graphs": [
                                      {
                                        "balloonText": "[[category]] at [[title]] : [[value]]",
                                        "fillAlphas": 1,
                                        "id": this.establishmentCompareDataArray[savedEstablishmentId].competitorEstablishmentName,
                                        "lineThickness": 1,
                                        "title": this.establishmentCompareDataArray[savedEstablishmentId].competitorEstablishmentName,
                                        "type": "column",
                                        "valueField": "column-1"
                                      },
                                      {
                                        "balloonText": "[[category]] at [[title]] : [[value]]",
                                        "fillAlphas": 1,
                                        "id": this.establishmentCompareDataArray[savedEstablishmentId].ownerEstablishmentName,
                                        "lineThickness": 1,
                                        "title": this.establishmentCompareDataArray[savedEstablishmentId].ownerEstablishmentName,
                                        "type": "column",
                                        "valueField": "column-2"
                                      }
                                    ],
                                    "guides": [],
                                    "valueAxes": [
                                      {
                                        "id": "ValueAxis-1",
                                        "stackType": "100%",
                                        "axisThickness": 0,
                                        "gridThickness": 0,
                                        "labelFrequency": 10,
                                        "showFirstLabel": false,
                                        "showLastLabel": false,
                                        "tickLength": 0,
                                        "title": "",
                                        "titleFontSize": 0,
                                        "titleRotation": 0
                                      }
                                    ],
                                    "allLabels": [],
                                    "balloon": {},
                                    "titles": [
                                      {
                                        "id": "Title-1",
                                        "size": 15,
                                        "text": ""
                                      }
                                    ],
                                    "dataProvider": [
                                      {
                                        "category": "Visitors",
                                        "column-1": this.establishmentCompareDataArray[savedEstablishmentId].competitorEstablishmentVisitsCount,
                                        "column-2": this.establishmentCompareDataArray[savedEstablishmentId].ownerEstablishmentVisitsCount
                                      }
                                    ]
                                });
                            },

                            // Error callback.
                            err => {
                                console.log("Something went wrong!");
                                console.log("Got error in API response");
                                console.log("Error details");
                                console.log(err);
                            }
                        );
                },

                // Error callback.
                err => {
                    console.log("Something went wrong!");
                    console.log("Got error in API response");
                    console.log("Error details");
                    console.log(err);
                }
            );
    }

    private calculateCompareStats(ownerEstablishmentName: string, ownerEstablishmentVisitsCount: number, competitorEstablishmentName: string, competitorEstablishmentVisitsCount: number): string {
        let lessOrMore: string = (competitorEstablishmentVisitsCount <= ownerEstablishmentVisitsCount) ? 'less' : 'more';

        let compareStatsMsg = '';
        if ((ownerEstablishmentVisitsCount === 0) && (competitorEstablishmentVisitsCount === 0)) {
            compareStatsMsg = 'Compare stats are not available.';
        } else {
            let compareStats = (ownerEstablishmentVisitsCount > 0) ? (Math.round((100 * Math.abs(ownerEstablishmentVisitsCount - competitorEstablishmentVisitsCount)) / ownerEstablishmentVisitsCount) + '% ' + lessOrMore) : ('100% ' + lessOrMore);
            compareStatsMsg = `${competitorEstablishmentName} has <span>${compareStats} Total Visitors</span> than your location, ${ownerEstablishmentName}.`;
        }

        return compareStatsMsg;
    }
}