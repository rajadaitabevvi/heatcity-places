import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { EstablishmentService } from './../services/establishment.service';
import { BizAccountService } from './../services/biz-account.service';
import { EstablishmentsData, HoursOfOperation } from './api-response-types/establishments-data';
import { BizAccount } from './types/bizAccount';
import { Account } from './../types/account';
import { IMG_BASE_URL } from './../config/constants';

@Component({
    selector: 'business-account',
    templateUrl: './business-account.component.html'
})

export class BusinessAccountComponent implements OnInit {
    establishmentId: string;

    bizAccountId: string;

    establishment: EstablishmentsData;

    bizAccount: BizAccount;

    account: Account;

    activeTab: string;

    establishmentStatus: string;

    bizAccountStatus: string;

    imgBaseUrl: string;

    constructor(
        private establishmentService: EstablishmentService,
        private bizAccountService: BizAccountService,
        private activatedRoute: ActivatedRoute
    ) {
        this.establishmentId = this.bizAccountService.getEstablishmentId();
        this.bizAccountId = this.bizAccountService.getBizAccountId();
        this.account = new Account();
        this.establishment = new EstablishmentsData();
        this.bizAccount = new BizAccount();
        this.activeTab = 'general-details';
        this.establishmentStatus = "";
        this.bizAccountStatus = "";
        this.imgBaseUrl = IMG_BASE_URL;
    }

    ngOnInit(): void {
        this.loadEstablishmentData();
        this.loadAdminInfo();
    }

    loadEstablishmentData(): void {
        this.establishmentService
        .getEstablishment(this.establishmentId)
        .subscribe(
            data => {
                console.log("loaded establishment data successfully");
                // Set establishment data got from the API response.
                this.establishment.id = data.id;
                this.establishment.name = (data.name) ? data.name : 'Not Available';
                this.establishment.address = (data.address) ? data.address : 'Not Available';
                this.establishment.url = (data.url) ? data.url : 'Not Available';
                this.establishment.capacity = (data.capacity) ? data.capacity : 0;
                this.establishment.pricing = (data.pricing) ? data.pricing : 0;
                this.establishment.description = (data.description) ? data.description : 'Not Available';
                this.establishment.hcActive = data.hcActive;

                if (this.establishment.hcActive === undefined) {
                    this.establishmentStatus = 'Not set';
                } else if (this.establishment.hcActive == true) {
                    this.establishmentStatus = 'Active';
                } else {
                    this.establishmentStatus = 'Inactive/Blocked';
                }

                this.establishment.hcCategory = (data.hcCategory) ? data.hcCategory : 'Not Available';
                this.establishment.hcCategories = (data.hcCategories) ? data.hcCategories : [];
                this.establishment.hcCategoriesCSV = this.establishment.hcCategories.join();
                this.establishment.hcRating = (data.hcRating) ? data.hcRating : 0;
                this.establishment.operatesAt = (data.operatesAt) ? data.operatesAt : new HoursOfOperation();
                this.establishment.photos = data.photos;

                // Load business account.
                this.bizAccountService
                .findUserById(this.bizAccountId)
                .subscribe(
                    data => {
                        console.log("loaded business account data successfully");
                        this.bizAccount.email = data.email;
                        this.bizAccount.phoneNumber = data.phoneNumber;
                        this.bizAccount.hcActive = data.hcActive;
                        if (this.bizAccount.hcActive === undefined) {
                            this.bizAccountStatus = "Not set";
                        } else if (this.bizAccount.hcActive == 0) {
                            this.bizAccountStatus = "Inactive/Block";
                        } else if (this.bizAccount.hcActive == 1) {
                            this.bizAccountStatus = "Approval Pending";
                        } else {
                            this.bizAccountStatus = "Active";
                        }
                    },
                    error => {
                        console.log('Error while getting business account data');
                        console.log(error);
                    }
                );
            },
            error => {
                console.log('Error while getting Establishment data');
                console.log(error);
            }
        );
    }

    loadAdminInfo(): void {
        this.establishmentService
        .getEstablishmentAdminInfo(this.establishmentId)
        .subscribe(
            data => {
                console.log("loaded admin info successfully");
                this.account.email = (data.email) ? data.email : 'Not Available';
                this.account.phoneNumber = (data.phoneNumber) ? data.phoneNumber : 'Not Available';
            },
            error => {
                console.log('Error while getting Establishment Admin Info data');
                console.log(error);
            }
        );
    }

    setTab(tab: string): void {
        this.activeTab = tab;
    }
}