import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';
import { API_BASE_URL, IMG_BASE_URL } from './../config/constants';
import { EstablishmentService } from './../services/establishment.service';
import { SavedEstablishmentData } from './api-response-types/saved-establishment-data';
import { DateRange } from './types/date-range';
import { EstablishmentVisitsCount } from './api-response-types/establishment-visits-count';
import { BizAccountService } from './../services/biz-account.service';
import { EstablishmentsData } from './api-response-types/establishments-data';

@Component({
    selector: 'establishment-detail',
    templateUrl: './establishment-detail.component.html'
})

export class EstablishmentDetailComponent implements OnInit {
    establishmentId: string;

    bizAccountId: string;

    userEstablishment: EstablishmentsData;

    userEstablishmentVisitsCount: number;

    selectedEstablishment: EstablishmentsData;

    selectedEstablishmentVisitsCount: number;

    saveSearchLabel: string;

    saveSearchFaIcon: string;

    amChart: AmChart;

    dateRange: DateRange;
    // Notification text related to graph data (loading/error handling etc.).
    graphNotifications: string;

    comparisonStats: string;

    graphReady: boolean;

    imgBaseUrl: string;

    isSaved: boolean;

    constructor(
        private http: HttpClient,
        private establishmentService: EstablishmentService,
        private amChartsService: AmChartsService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private bizAccountService: BizAccountService
    ) {
        this.establishmentId = this.bizAccountService.getEstablishmentId();
        this.bizAccountId = this.bizAccountService.getBizAccountId();
        this.userEstablishment = new EstablishmentsData();
        this.selectedEstablishment = new EstablishmentsData();
        this.selectedEstablishmentVisitsCount = 0;
        this.userEstablishmentVisitsCount = 0;
        this.saveSearchLabel = 'Save this search';
        this.saveSearchFaIcon = 'fa fa-star-o';
        this.dateRange = new DateRange();
        this.graphNotifications = 'Submit start date and end date to check the visits graph.';
        this.comparisonStats = '';
        this.graphReady = false;
        this.imgBaseUrl = IMG_BASE_URL;
        this.isSaved = false;
    }

    ngOnInit(): void {
        this.loadSelectedEstablishment();
        this.loadUserEstablishment();
    }

    loadSelectedEstablishment(): void {
        this.activatedRoute.params.subscribe((params: Params) => {
            if (params['id'] !== undefined) {
                // Check if this establishment is saved or not.
                this.establishmentService
                .getSavedEstablishments(this.bizAccountId)
                .subscribe(
                    data => {
                        data.forEach(element => {
                            if (params['id'] == element.establishmentId) {
                                this.isSaved = true;
                            }
                        });
                    },
                    error => {
                        console.log('Error while getting saved establishments data');
                        console.log(error);
                    }
                );

                this.establishmentService
                .getEstablishment(params['id'])
                .subscribe(
                    data => {
                        this.selectedEstablishment = data;
                    },
                    error => {
                        console.log('Error while getting Establishment data');
                        console.log(error);
                    }
                );
            }
        });
    }

    loadUserEstablishment(): void {
        this.establishmentService
        .getEstablishment(this.establishmentId)
        .subscribe(
            data => {
                this.userEstablishment = data;
            },
            error => {
                console.log('Error while getting Establishment data');
                console.log(error);
            }
        );
    }

    saveSearchedEstablishment(): void {
        let establishmentData: SavedEstablishmentData = new SavedEstablishmentData();
        establishmentData.bizaccountId = this.bizAccountId;
        establishmentData.establishmentId = this.selectedEstablishment.id;

        this.establishmentService
        .saveSearchedEstablishment(establishmentData)
        .subscribe(
            data => {
                this.saveSearchLabel = 'Saved';
                this.saveSearchFaIcon = 'fa fa-star';
                console.log('Searched Establishment saved to your account successfully.');
                console.log(data);
            },
            error => {
                console.log('Error while saving searched Establishment data into the account.');
                console.log(error);
            }
        )
    }

    ngOnDestroy(): void {
        if (this.amChart) {
            this.amChartsService.destroyChart(this.amChart);
        }
    }

    renderGraph(): void {
        if (this.amChart) {
            this.amChartsService.destroyChart(this.amChart);
        }
        this.graphNotifications = 'Loading...';

        // Get user establishment visits count.
        this.http
            .get<EstablishmentVisitsCount>(`${API_BASE_URL}/api/establishments/${this.establishmentId}/visits?startDate=${this.dateRange.startDate}&endDate=${this.dateRange.endDate}`)
            .subscribe(
                // Success callback.
                data => {
                    this.userEstablishmentVisitsCount = data.count;

                    // Get selected establishment visits count.
                    this.http
                        .get<EstablishmentVisitsCount>(`${API_BASE_URL}/api/establishments/${this.selectedEstablishment.id}/visits?startDate=${this.dateRange.startDate}&endDate=${this.dateRange.endDate}`)
                        .subscribe(
                            // Success callback.
                            data => {
                                this.selectedEstablishmentVisitsCount = data.count;

                                this.graphNotifications = '';
                                this.graphReady = true;

                                // Calculate comparison statistics.
                                this.comparisonStats = this.calculateCompareStats(this.userEstablishmentVisitsCount, this.selectedEstablishmentVisitsCount);

                                // Render graph data.
                                this.amChart = this.amChartsService.makeChart("chartdiv",
                                {
                                    "type": "serial",
                                    "categoryField": "category",
                                    "rotate": true,
                                    "colors": [
                                      "#cd3535",
                                      "#658396",
                                      "#B0DE09",
                                      "#0D8ECF",
                                      "#2A0CD0",
                                      "#CD0D74",
                                      "#CC0000",
                                      "#00CC00",
                                      "#0000CC",
                                      "#DDDDDD",
                                      "#999999",
                                      "#333333",
                                      "#990000"
                                    ],
                                    "categoryAxis": {
                                      "gridPosition": "start",
                                      "autoGridCount": false,
                                      "axisThickness": 0,
                                      "gridCount": 0,
                                      "gridThickness": 0,
                                      "minVerticalGap": 30,
                                      "showFirstLabel": false,
                                      "showLastLabel": false,
                                      "tickLength": 0
                                    },
                                    "trendLines": [],
                                    "graphs": [
                                      {
                                        "balloonText": "[[category]] at [[title]] : [[value]]",
                                        "fillAlphas": 1,
                                        "id": this.selectedEstablishment.name,
                                        "title": this.selectedEstablishment.name,
                                        "type": "column",
                                        "valueField": "column-1"
                                      },
                                      {
                                        "balloonText": "[[category]] at [[title]] : [[value]]",
                                        "fillAlphas": 1,
                                        "id": this.userEstablishment.name,
                                        "title": this.userEstablishment.name,
                                        "type": "column",
                                        "valueField": "column-2"
                                      }
                                    ],
                                    "guides": [],
                                    "valueAxes": [
                                      {
                                        "id": "ValueAxis-1",
                                        "stackType": "100%",
                                        "axisThickness": 0,
                                        "gridThickness": 0,
                                        "labelFrequency": 10,
                                        "showFirstLabel": false,
                                        "showLastLabel": false,
                                        "tickLength": 0,
                                        "title": "",
                                        "titleFontSize": 0,
                                        "titleRotation": 0
                                      }
                                    ],
                                    "allLabels": [],
                                    "balloon": {},
                                    "titles": [
                                      {
                                        "id": "Title-1",
                                        "size": 15,
                                        "text": ""
                                      }
                                    ],
                                    "dataProvider": [
                                      {
                                        "category": "Visitors",
                                        "column-1": this.selectedEstablishmentVisitsCount,
                                        "column-2": this.userEstablishmentVisitsCount
                                      }
                                    ]
                                });
                            },

                            // Error callback.
                            err => {
                                this.graphNotifications = 'Problem loading the graph data. Please try again after some time.';
                                console.log("Something went wrong!");
                                console.log("Got error in API response");
                                console.log("Error details");
                                console.log(err);
                            }
                        );
                },

                // Error callback.
                err => {
                    console.log("Something went wrong!");
                    console.log("Got error in API response");
                    console.log("Error details");
                    console.log(err);
                }
            );
    }

    private calculateCompareStats(ownerEstablishmentVisitsCount, competitorEstablishmentVisitsCount): string {
        let lessOrMore: string = (competitorEstablishmentVisitsCount <= ownerEstablishmentVisitsCount) ? 'less' : 'more';

        let compareStatsMsg = '';
        if ((ownerEstablishmentVisitsCount === 0) && (competitorEstablishmentVisitsCount === 0)) {
            compareStatsMsg = 'Compare stats are not available.';
        } else {
            let compareStats = (ownerEstablishmentVisitsCount > 0) ? (Math.round((100 * Math.abs(ownerEstablishmentVisitsCount - competitorEstablishmentVisitsCount)) / ownerEstablishmentVisitsCount) + '% ' + lessOrMore) : ('100% ' + lessOrMore);
            compareStatsMsg = `${this.selectedEstablishment.name} has ${compareStats} Total Visitors than your location, ${this.userEstablishment.name}.`;
        }

        return compareStatsMsg;
    }
}