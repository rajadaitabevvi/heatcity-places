import { Component } from '@angular/core';
import { API_BASE_URL } from './../config/constants';
import { PromotionService } from './../services/promotion.service';
import { BizAccountService } from './../services/biz-account.service';
import { PromotionResponseType } from './api-response-types/promotion-response-type';

@Component({
    selector: 'promotions',
    templateUrl: './promotions.component.html'
})

export class PromotionsComponent {
    establishmentId: string;
    promotions: PromotionResponseType[];
    notificationMessage: string;
    sortyBy: string;

    constructor(
        private promotionService: PromotionService,
        private bizAccountService: BizAccountService
    ) {
        this.establishmentId = this.bizAccountService.getEstablishmentId();
        this.promotions = [];
        this.notificationMessage = 'Loading...';
        this.sortyBy = 'revenue';
    }

    ngOnInit(): void {
        this.loadPromotions();
    }

    loadPromotions(): void {
        this.promotionService
        .getPromotions(this.establishmentId, this.sortyBy)
        .subscribe(
            data => {
                this.notificationMessage = '';
                this.promotions = data;
                if (this.promotions.length == 0) {
                    this.notificationMessage = 'No promotions found.';
                }
            },
            error => {
                this.notificationMessage = 'An error encountered while getting promotions data.';
                console.log(error);
            }
        );
    }

    private createDateObj(dateString: string): Date {
        return new Date(dateString);
    }

    sortPromotions(sortyByValue: string): void {
        // Unset the current promotions list.
        this.promotions = [];

        // Set the sort by value.
        this.sortyBy = sortyByValue;

        // Load promotions.
        this.loadPromotions();
    }
}