import { Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { AnalyticsVisitorsComponent } from './analytics-visitors.component';
import { AnalyticsTimeComponent } from './analytics-time.component';
import { AnalyticsDemographicComponent } from './analytics-demographic.component';
import { CompareEstablishmentsComponent } from './compare-establishments.component';
import { EstablishmentDetailComponent } from './establishment-detail.component';
import { SavedEstablishmentsComponent } from './saved-establishments.component';
import { CreatePromotionComponent } from './create-promotion.component';
import { PromotionsComponent } from './promotions.component';
import { PromotionsCalendarComponent } from './promotions-calendar.component';
import { BusinessAccountComponent } from './business-account.component';
import { UpdateBusinessAccountComponent } from './update-business-account.component';
import { UserAuthGuard } from './../guards/user-auth.guard';

export const BACKEND_ROUTES: Routes = [
    {
        path: 'account/dashboard',
        component: DashboardComponent,
        canActivate: [UserAuthGuard]
    },
    {
        path: 'account/analytics/visitors',
        component: AnalyticsVisitorsComponent,
        canActivate: [UserAuthGuard]
    },
    {
        path: 'account/analytics/time-spent',
        component: AnalyticsTimeComponent,
        canActivate: [UserAuthGuard]
    },
    {
        path: 'account/analytics/demographic',
        component: AnalyticsDemographicComponent,
        canActivate: [UserAuthGuard]
    },
    {
        path: 'account/compare/search',
        component: CompareEstablishmentsComponent,
        canActivate: [UserAuthGuard]
    },
    {
        path: 'account/compare/saved-searches',
        component: SavedEstablishmentsComponent,
        canActivate: [UserAuthGuard]
    },
    {
        path: 'account/promotions/create',
        component: CreatePromotionComponent,
        canActivate: [UserAuthGuard]
    },
    {
        path: 'account/promotions/repeat/:id',
        component: CreatePromotionComponent,
        canActivate: [UserAuthGuard]
    },
    {
        path: 'account/promotions/all',
        component: PromotionsComponent,
        canActivate: [UserAuthGuard]
    },
    {
        path: 'account/promotions/calendar',
        component: PromotionsCalendarComponent,
        canActivate: [UserAuthGuard]
    },
    {
        path: 'account/establishment',
        component: BusinessAccountComponent,
        canActivate: [UserAuthGuard]
    },
    {
        path: 'account/establishment/update',
        component: UpdateBusinessAccountComponent,
        canActivate: [UserAuthGuard]
    },
    {
        path: 'account/establishment/:id',
        component: EstablishmentDetailComponent,
        canActivate: [UserAuthGuard]
    }
];