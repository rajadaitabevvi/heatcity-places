import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { EstablishmentService } from './../services/establishment.service';
import { BizAccountService } from './../services/biz-account.service';
import { AccountService } from './../services/account.service';
import { EstablishmentsData, HoursOfOperation, OperatingHours } from './api-response-types/establishments-data';
import { BizAccount } from './types/bizAccount';
import { Account } from './../types/account';

@Component({
    selector: 'update-business-account',
    templateUrl: './update-business-account.component.html'
})

export class UpdateBusinessAccountComponent implements OnInit {
    establishmentId: string;

    bizAccountId: string;

    establishment: EstablishmentsData;

    bizAccount: BizAccount;

    account: Account;

    activeTab: string;

    notificationMessage: string;
    
    submitted: boolean;
    
    establishmentUpdated: boolean;

    timeOptions: string[];

    constructor(
        private establishmentService: EstablishmentService,
        private bizAccountService: BizAccountService,
        private accountService: AccountService,
        private activatedRoute: ActivatedRoute
    ) {
        this.establishmentId = this.bizAccountService.getEstablishmentId();
        this.bizAccountId = this.bizAccountService.getBizAccountId();
        this.account = new Account();
        this.establishment = new EstablishmentsData();
        this.bizAccount = new BizAccount();
        this.activeTab = 'general-details';
        this.notificationMessage = "";
        this.submitted = false;
        this.establishmentUpdated = false;
        this.timeOptions = [
            "12:00 am",
            "12:30 am",
            "01:00 am",
            "01:30 am",
            "02:00 am",
            "02:30 am",
            "03:00 am",
            "03:30 am",
            "04:00 am",
            "04:30 am",
            "05:00 am",
            "05:30 am",
            "06:00 am",
            "06:30 am",
            "07:00 am",
            "07:30 am",
            "08:00 am",
            "08:30 am",
            "09:00 am",
            "09:30 am",
            "10:00 am",
            "10:30 am",
            "11:00 am",
            "11:30 am",
            "12:00 pm",
            "12:30 pm",
            "01:00 pm",
            "01:30 pm",
            "02:00 pm",
            "02:30 pm",
            "03:00 pm",
            "03:30 pm",
            "04:00 pm",
            "04:30 pm",
            "05:00 pm",
            "05:30 pm",
            "06:00 pm",
            "06:30 pm",
            "07:00 pm",
            "07:30 pm",
            "08:00 pm",
            "08:30 pm",
            "09:00 pm",
            "09:30 pm",
            "10:00 pm",
            "10:30 pm",
            "11:00 pm",
            "11:30 pm"
        ];
    }

    ngOnInit(): void {
        this.loadEstablishmentData();
        this.loadAdminInfo();
    }

    loadEstablishmentData(): void {
        this.establishmentService
        .getEstablishment(this.establishmentId)
        .subscribe(
            data => {
                console.log("loaded establishment data successfully");
                // Set establishment data got from the API response.
                this.establishment.id = data.id;
                this.establishment.name = (data.name) ? data.name : 'Not Available';
                this.establishment.address = (data.address) ? data.address : 'Not Available';
                this.establishment.url = (data.url) ? data.url : 'Not Available';
                this.establishment.capacity = (data.capacity) ? data.capacity : 0;
                this.establishment.pricing = (data.pricing) ? data.pricing : 0;
                this.establishment.description = (data.description) ? data.description : 'Not Available';
                this.establishment.hcActive = data.hcActive;
                this.establishment.hcCategory = (data.hcCategory) ? data.hcCategory : 'Not Available';
                this.establishment.hcCategories = (data.hcCategories) ? data.hcCategories : [];
                this.establishment.hcCategoriesCSV = this.establishment.hcCategories.join();
                this.establishment.hcRating = (data.hcRating) ? data.hcRating : 0;
                this.establishment.operatesAt = (data.operatesAt) ? data.operatesAt : new HoursOfOperation();

                // Load business account.
                this.bizAccountService
                .findUserById(this.bizAccountId)
                .subscribe(
                    data => {
                        console.log("loaded business account data successfully");
                        this.bizAccount.id = data.id;
                        this.bizAccount.email = data.email;
                        this.bizAccount.phoneNumber = data.phoneNumber;
                        this.bizAccount.hcActive = data.hcActive;
                    },
                    error => {
                        console.log('Error while getting business account data');
                        console.log(error);
                    }
                );
            },
            error => {
                console.log('Error while getting Establishment data');
                console.log(error);
            }
        );
    }

    loadAdminInfo(): void {
        this.establishmentService
        .getEstablishmentAdminInfo(this.establishmentId)
        .subscribe(
            data => {
                console.log("loaded admin info successfully");
                this.account.id = data.id;
                this.account.email = (data.email) ? data.email : 'Not Available';
                this.account.phoneNumber = (data.phoneNumber) ? data.phoneNumber : 'Not Available';
            },
            error => {
                console.log('Error while getting Establishment Admin Info data');
                console.log(error);
            }
        );
    }

    setTab(tab: string): void {
        this.activeTab = tab;
    }

    updateEstablishment(): void {
        this.submitted = true;

        // Get submitted establishment data.
        let establishmentData = {
            "name": this.establishment.name,
            "address": this.establishment.address,
            "url": this.establishment.url,
            "capacity": this.establishment.capacity,
            "pricing": this.establishment.pricing,
            "description": this.establishment.description,
            "hcCategory": this.establishment.hcCategory,
            "hcCategories": this.establishment.hcCategoriesCSV.split(','),
            "hcRating": this.establishment.hcRating,
            "operatesAt": this.establishment.operatesAt,
            // Set establishment status.
            "hcActive": this.establishment.hcActive,
            "locationId": "0"
        };

        // First update an establishment.
        this.establishmentService
        .updateEstablishment(this.establishmentId, establishmentData)
        .subscribe(
            data => {
                console.log("establishment is updated successfully");

                // Get business account data.
                let bizAccountData = {
                    "email": this.bizAccount.email,
                    "phoneNumber": this.bizAccount.phoneNumber,
                    // Set business account status.
                    "hcActive": this.bizAccount.hcActive
                };

                // Update a business account.
                this.bizAccountService
                .updateBizAccount(this.bizAccount.id, bizAccountData)
                .subscribe(
                    data => {
                        console.log("business account is updated successfully");

                        // Update an admin account.
                        let adminData = {
                            "email": this.account.email,
                            "phoneNumber": this.account.phoneNumber
                        };

                        this.accountService
                        .update(this.account.id, adminData)
                        .subscribe(
                            data => {
                                console.log("admin account is updated successfully");

                                this.establishmentUpdated = true;
                                this.notificationMessage = "Changes are saved successfully.";
                            },
                            (error: HttpErrorResponse) => {
                                if (error.status === 0) {
                                    // A client-side or network error occurred. Handle it accordingly.
                                    console.log(`A client-side error occurred: ${error.message}`);
                                    this.notificationMessage = "Request for update establishment failed. Please check your connection and try again later.";
                                } else {
                                    // The backend returned an unsuccessful response code.
                                    console.log(`Backend returned status code ${error.status}, message: ${error.error.error.message}`);
                                    //this.notificationMessage = error.error.error.message;
                                    this.notificationMessage = "Request for update establishment failed.";
                                }
                            }
                        );
                    },
                    (error: HttpErrorResponse) => {
                        if (error.status === 0) {
                            // A client-side or network error occurred. Handle it accordingly.
                            console.log(`A client-side error occurred: ${error.message}`);
                            this.notificationMessage = "Request for update establishment failed. Please check your connection and try again later.";
                        } else {
                            // The backend returned an unsuccessful response code.
                            console.log(`Backend returned status code ${error.status}, message: ${error.error.error.message}`);
                            //this.notificationMessage = error.error.error.message;
                            this.notificationMessage = "Request for update establishment failed.";
                        }
                    }
                );
            },
            (error: HttpErrorResponse) => {
                if (error.status === 0) {
                    // A client-side or network error occurred. Handle it accordingly.
                    console.log(`A client-side error occurred: ${error.message}`);
                    this.notificationMessage = "Request for update establishment failed. Please check your connection and try again later.";
                } else {
                    // The backend returned an unsuccessful response code.
                    console.log(`Backend returned status code ${error.status}, message: ${error.error.error.message}`);
                    //this.notificationMessage = error.error.error.message;
                    this.notificationMessage = "Request for update establishment failed.";
                }
            }
        );
    }
}