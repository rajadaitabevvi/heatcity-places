import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL, IMG_BASE_URL } from './../config/constants';
import { EstablishmentsData } from './api-response-types/establishments-data';

@Component({
    selector: 'compare-establishments',
    templateUrl: './compare-establishments.component.html'
})

export class CompareEstablishmentsComponent implements OnInit {
    notificationMessage: string;

    establishmentsData: EstablishmentsData[];

    pageSize: number;
    
    offset: number;

    showLoadMore: boolean;

    imgBaseUrl: string;

    constructor(
        private http: HttpClient
    ) {
        this.notificationMessage = 'Loading...';
        this.establishmentsData = [];
        this.pageSize = 9;
        this.offset = 0;
        this.showLoadMore = false;
        this.imgBaseUrl = IMG_BASE_URL;
    }

    ngOnInit(): void {
        this.loadEstablishments();
    }

    loadEstablishments(): void {
        this.notificationMessage = 'Loading...';

        this.http
        .get<EstablishmentsData[]>(`${API_BASE_URL}/api/establishments?filter={"limit":${this.pageSize},"offset":${this.offset},"include":"photos"}`)
        .subscribe(
            // Success callback.
            data => {
                // Clear the notification message.
                this.notificationMessage = '';

                // Push the data into establishmentsData array.
                data.forEach(element => {
                    this.establishmentsData.push(element);
                    console.log("element.photos.length");
                    console.log(element.photos.length);
                    if (element.photos.length > 0) {
                        console.log(element.photos[0].file.path);
                    }
                });

                // Update the offset.
                this.offset += this.pageSize;

                // Check if we need to show "Load More" button/link.
                this.showLoadMore = (data.length < this.pageSize) ? false : true;
            },

            // Error callback.
            err => {
                this.notificationMessage = 'Failed to load establishments data.';
                console.log("Something went wrong!");
                console.log("Got error in API response");
                console.log("Error details");
                console.log(err);
            }
        );
    }

    loadMore(): void {
        this.loadEstablishments();
    }
}