import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstablishmentService } from './../services/establishment.service';
import { AuthService } from './../services/auth.service';
import { BizAccountService } from './../services/biz-account.service';
import { EstablishmentsData } from './../backend/api-response-types/establishments-data';
import { IMG_BASE_URL } from './../config/constants';

@Component({
    selector: 'backend',
    templateUrl: './backend.component.html'
})

export class BackendComponent implements OnInit {
    establishmentId: string;

    userEstablishment: EstablishmentsData;

    imgBaseUrl: string;

    constructor(
        private establishmentService: EstablishmentService,
        private authService: AuthService,
        private bizAccountService: BizAccountService,
        private router: Router
    ) {
        this.establishmentId = this.bizAccountService.getEstablishmentId();
        this.userEstablishment = new EstablishmentsData();
        this.imgBaseUrl = IMG_BASE_URL;
    }

    ngOnInit(): void {
        this.loadEstablishmentData();
    }

    loadEstablishmentData(): void {
        this.establishmentService
            .getEstablishment(this.establishmentId)
            .subscribe(
                data => {
                    this.userEstablishment = data;
                },
                error => {
                    console.log('Error while getting Establishment data');
                    console.log(error);
                }
            );
    }

    doSignOut(): void {
        this.authService.bizAccountLogout();

        // Navigate user to the Home page.
        this.router.navigate(['/']);
    }
}