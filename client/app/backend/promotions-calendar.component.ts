import { Component } from '@angular/core';
import { API_BASE_URL } from './../config/constants';
import { PromotionService } from './../services/promotion.service';
import { BizAccountService } from './../services/biz-account.service';
import { PromotionResponseType } from './api-response-types/promotion-response-type';
import * as moment from 'moment';
declare var jquery:any;
declare var $ :any;

@Component({
    selector: 'promotions-calendar',
    templateUrl: './promotions-calendar.component.html'
})

export class PromotionsCalendarComponent {
    establishmentId: string;
    promotions: PromotionResponseType[];
    notificationMessage: string;

    constructor(
        private promotionService: PromotionService,
        private bizAccountService: BizAccountService
    ) {
        this.establishmentId = this.bizAccountService.getEstablishmentId();
        this.promotions = [];
        this.notificationMessage = 'Loading...';
    }

    ngOnInit(): void {
        // Setup Calendar.
        $(document).ready(function () {
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                defaultDate: moment().format("YYYY-MM-DD"),
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                eventLimit: true, // allow "more" link when too many events
            });
        });

        // Load promotions data.
        this.loadPromotions();
    }

    loadPromotions(): void {
        this.promotionService
        .getPromotions(this.establishmentId)
        .subscribe(
            data => {
                this.promotions = data;
                this.notificationMessage = '';
                if (this.promotions.length == 0) {
                    this.notificationMessage = 'No promotions found.';
                }

                let promoEvents: Object[] = [];
                this.promotions.forEach(element => {
                    promoEvents.push({
                        title: element.name,
                        start: element.startsAt,
                        end: element.endsAt,
                        allDay: false
                    });
                });

                // Add event source to the calendar.
                $('#calendar').fullCalendar('addEventSource', promoEvents);
            },
            error => {
                this.notificationMessage = 'An error encountered while getting promotions data.';
                console.log(error);
            }
        );
    }
}