export interface AgeRangeData {
    age: string;
    ageCount: number;
    min: number;
    max: number;
}

export interface GenderStats {
    male: number;
    female: number;
}

interface totalData {
    ages: AgeRangeData[];
    gender: GenderStats;
}

export interface AnalyticsPieChartData {
    total: totalData;
}