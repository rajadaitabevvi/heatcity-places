export interface PromotionResponseType {
    attachments: Object[];
    name: string;
    description: string;
    amount: number;
    startsAt: string;
    endsAt: string;
    areaName: string;
    promoRadius: number;
    ageRange: string;
    deletedAt: string;
    targeted: boolean;
    establishmentId: string;
    clicks: number;
    promotionVists: number;
    active: boolean;
    id: string;
    createdAt: string;
    updatedAt: string;
}