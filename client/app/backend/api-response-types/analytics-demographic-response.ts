interface DemographicData {
    ages: AgeRangeData[];
    gender: GenderStats;
}

export interface AgeRangeData {
    age: string;
    ageCount: number;
    min: number;
    max: number;
}

interface GenderStats {
    male: number;
    female: number;
}

export interface AnalyticsDemographicResponse {
    today: DemographicData;
    week: DemographicData;
    month: DemographicData;
    year: DemographicData;
}