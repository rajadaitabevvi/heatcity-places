export class SavedEstablishmentData {    
    constructor(
        public establishmentId: string = '',
        public bizaccountId: string = ''
    ) {}
}