export class OperatingHours {
    constructor(
        public startTime: string = "12:00 am",
        public endTime: string = "12:00 am"
    ) {}
}

export class HoursOfOperation {
    constructor(
        public mon: OperatingHours = new OperatingHours(),
        public tue: OperatingHours = new OperatingHours(),
        public wed: OperatingHours = new OperatingHours(),
        public thu: OperatingHours = new OperatingHours(),
        public fri: OperatingHours = new OperatingHours(),
        public sat: OperatingHours = new OperatingHours(),
        public sun: OperatingHours = new OperatingHours()
    ) {}
}

export class File {
    constructor(
        public filename: string = "",
        public path: string = ""
    ) {}
}

export class Photo {
    constructor(
        public file: File = new File(),
        public id: string = "",
        public establishmentId: string = "",
        public accountId: string = "",
        public createdAt: string = "",
        public updatedAt: string = ""
    ) {}
}

export interface Establishment {
    id: string;
    name: string;
    address: string;
    hcCategory: string;
    pricing: number;
    capacity: number;
    description: string;
    phoneNumber: string;
    email: string;
    url: string;
    rating: number;
    categories: string[];
    operatesAt: HoursOfOperation;
    popularAt: Object;
    deletedAt: string;
    accountId: string;
    locationId: string;
    visits: Object;
    geoLocation: Object;
    menu: Object;
    hcActive: boolean;
    hcCategories: string[];
    hcRating: number;
    deleted: boolean;
    parentOrgId: string;
    createdAt: string;
    updatedAt: string;
    areaId: string;
    bizAccountId: string;
    photos: Photo[];
}