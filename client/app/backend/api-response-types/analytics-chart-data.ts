export interface DateWiseVisits {
    date: string;
    visits: number;
}

export interface DateWiseTimes {
    date: string;
    time: number;
}

export interface AnalyticsChartData {
    vC: Object;
    tS: Object;
    visitCount: DateWiseVisits[];
    timeSpent: DateWiseTimes[];
}