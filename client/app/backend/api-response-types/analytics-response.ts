interface VisitedStats {
    today: number;
    thisWeek: number;
    thisMonth: number;
    thisYear: number;
    total: number;
}

interface TimeStats {
    min: number;
    max: number;
    average: number;
}

interface TimeSpentStats {
    today: number;
    thisWeek: number;
    thisMonth: number;
    thisYear: number;
}

interface CrowdStats {
    current: number;
}

export interface AgeRangeData {
    age: string;
    ageCount: number;
    min: number;
    max: number;
}

interface GenderStats {
    male: number;
    female: number;
}

export interface AnalyticsResponse {
    displayAges: boolean;
    ages: AgeRangeData[];
    heatHours: Object[][];
    gender: GenderStats;
    crowd: CrowdStats;
    capacity: number;
    visited: VisitedStats;
    timeSpent: TimeSpentStats;
    time: TimeStats;
}