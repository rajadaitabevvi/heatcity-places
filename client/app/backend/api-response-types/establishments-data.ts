export class OperatingHours {
    constructor(
        public startTime: string = "12:00 am",
        public endTime: string = "12:00 am"
    ) {}
}

export class HoursOfOperation {
    constructor(
        public mon: OperatingHours = new OperatingHours(),
        public tue: OperatingHours = new OperatingHours(),
        public wed: OperatingHours = new OperatingHours(),
        public thu: OperatingHours = new OperatingHours(),
        public fri: OperatingHours = new OperatingHours(),
        public sat: OperatingHours = new OperatingHours(),
        public sun: OperatingHours = new OperatingHours()
    ) {}
}

export class File {
    constructor(
        public filename: string = "",
        public path: string = ""
    ) {}
}

export class Photo {
    constructor(
        public file: File = new File(),
        public id: string = "",
        public establishmentId: string = "",
        public accountId: string = "",
        public createdAt: string = "",
        public updatedAt: string = ""
    ) {}
}

export class EstablishmentsData {
    constructor(
        public name: string = "",
        public description: string = "",
        public phoneNumber: string = "",
        public email: string = "",
        public url: string = "",
        public pricing: number = 0,
        public rating: number = 0,
        public categories: string[] = [],
        public operatesAt: HoursOfOperation = new HoursOfOperation(),
        public popularAt: Object = {},
        public deletedAt: string = "",
        public accountId: string = "",
        public locationId: string = "",
        public visits: Object = {},
        public geoLocation: Object = {},
        public menu: Object = {},
        public address: string = "",
        public hcActive: boolean = true,
        public hcCategory: string = "",
        public hcCategories: string[] = [],
        public hcCategoriesCSV: string = "",
        public hcRating: number = 0,
        public deleted: boolean = false,
        public parentOrgId: string = "",
        public capacity: number = 0,
        public id: string = "",
        public createdAt: string = "",
        public updatedAt: string = "",
        public areaId: string = "",
        public bizAccountId: string = "",
        public photos: Photo[] = []
    ) {}

    reset(): void {
        this.name = "";
        this.description = "";
        this.phoneNumber = "";
        this.email = "";
        this.url = "";
        this.pricing = 0;
        this.rating = 0;
        this.categories = [];
        this.operatesAt = new HoursOfOperation();
        this.popularAt = {};
        this.deletedAt = "";
        this.accountId = "";
        this.locationId = "";
        this.visits = {};
        this.geoLocation = {};
        this.menu = {};
        this.address = "";
        this.hcActive = true;
        this.hcCategory = "";
        this.hcCategories = [];
        this.hcCategoriesCSV = "";
        this.hcRating = 0;
        this.deleted = false;
        this.parentOrgId = "";
        this.capacity = 0;
        this.id = "";
        this.createdAt = "";
        this.updatedAt = "";
        this.areaId = "";
        this.bizAccountId = "";
        this.photos = [];
    }
}