import { Component, OnInit } from '@angular/core';
import { API_BASE_URL, MONTHS } from './../config/constants';
import { HttpClient } from '@angular/common/http';
import { AnalyticsResponse } from './api-response-types/analytics-response';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';
import { DateRange } from './types/date-range';
import { AnalyticsChartData, DateWiseVisits } from './api-response-types/analytics-chart-data';
import { BizAccountService } from './../services/biz-account.service';

@Component({
    selector: 'analytics-visitors',
    templateUrl: './analytics-visitors.component.html'
})

export class AnalyticsVisitorsComponent implements OnInit {
    establishmentId: string;

    today: string;

    thisWeek: string;

    thisMonth: string;

    thisYear: string;

    todayVisitorsCount: number;

    thisWeekVisitorsCount: number;

    thisMonthVisitorsCount: number;

    thisYearVisitorsCount: number;

    amChart: AmChart;

    dateRange: DateRange;

    // Notification text related to graph data (loading/error handling etc.).
    graphNotifications: string;

    constructor(
        private http: HttpClient,
        private amChartsService: AmChartsService,
        private bizAccountService: BizAccountService
    ) {
        this.establishmentId = this.bizAccountService.getEstablishmentId();

        this.setTimingStrings();

        this.todayVisitorsCount = 0;
        this.thisWeekVisitorsCount = 0;
        this.thisMonthVisitorsCount = 0;
        this.thisYearVisitorsCount = 0;

        this.dateRange = new DateRange();

        this.graphNotifications = 'Submit start date and end date to check the visits graph.';
    }

    setTimingStrings(): void {
        let currentDate = new Date();
        let currentYear = new Date().getFullYear();
        let currentMonth = new Date().getMonth();

        // Set "today"
        // weekday along with long date: Wednesday, October 4, 2017
        let options = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        this.today = currentDate.toLocaleString("en-US", options);

        // Set "thisWeek"
        // Sunday - Monday
        this.thisWeek = "Sunday - Monday";

        // Set "thisMonth"
        // October 2017
        this.thisMonth = `${MONTHS[currentMonth]} ${currentYear}`;

        // Set "thisYear"
        // Jan 2017 - Today
        this.thisYear = `${MONTHS[0]} ${currentYear} - Today`;
    }

    ngOnInit(): void {
        this.loadVisitorsStats();
    }

    loadVisitorsStats(): void {
        console.log('loadVisitorsStats calling API');
        console.log(`${API_BASE_URL}/api/establishments/${this.establishmentId}/statistics`);

        this.http
            .get<AnalyticsResponse>(`${API_BASE_URL}/api/establishments/${this.establishmentId}/statistics`)
            .subscribe(
                // Success callback.
                data => {
                    console.log("success callback");
                    console.log("data.visited");
                    console.log(data.visited);
                    // Set visitor statistics.
                    this.todayVisitorsCount = (data.visited && data.visited.today) ? data.visited.today : 0;
                    this.thisWeekVisitorsCount = (data.visited && data.visited.thisWeek) ? data.visited.thisWeek : 0;
                    this.thisMonthVisitorsCount = (data.visited && data.visited.thisMonth) ? data.visited.thisMonth : 0;
                    this.thisYearVisitorsCount = (data.visited && data.visited.thisYear) ? data.visited.thisYear : 0;
                },

                // Error callback.
                err => {
                    console.log("Something went wrong!");
                    console.log("Got error in API response");
                    console.log("Error details");
                    console.log(err);
                }
            );
    }

    ngOnDestroy(): void {
        if (this.amChart) {
            this.amChartsService.destroyChart(this.amChart);
        }
    }

    renderGraph(): void {
        console.log('renderGraph calling API');
        console.log(`${API_BASE_URL}/api/establishments/${this.establishmentId}/graphStats?startDate=${this.dateRange.startDate}&endDate=${this.dateRange.endDate}`);

        if (this.amChart) {
            this.amChartsService.destroyChart(this.amChart);
        }
        this.graphNotifications = 'Loading...';

        // Get graph data from an API.
        this.http
            .get<AnalyticsChartData>(`${API_BASE_URL}/api/establishments/${this.establishmentId}/graphStats?startDate=${this.dateRange.startDate}&endDate=${this.dateRange.endDate}`)
            .subscribe(
                // Success callback.
                data => {
                    console.log("success callback");
                    console.log("data.visitCount");
                    console.log(data.visitCount);

                    let visitsGraphData: DateWiseVisits[];
                    visitsGraphData = data.visitCount;

                    if (visitsGraphData.length > 0) {
                        this.graphNotifications = '';

                        for (var i = 0; i < visitsGraphData.length; i++) {
                            visitsGraphData[i].date = this.formatDate(visitsGraphData[i].date);
                        }

                        // Render graph data.
                        this.amChart = this.amChartsService.makeChart("chartdiv",
                        {
                            "type": "serial",
                            "theme": "light",
                            "marginRight": 30,
                            "legend": {
                                "equalWidths": false,
                                "periodValueText": "total: [[value.sum]]",
                                "position": "top",
                                "valueAlign": "left",
                                "valueWidth": 100
                            },
                            "dataProvider": visitsGraphData,
                            "valueAxes": [{
                                "stackType": "regular",
                                "gridAlpha": 0.07,
                                "position": "left",
                                "title": "Visits"
                            }],
                            "graphs": [{
                                "balloonText": "<span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
                                "fillAlphas": 0.6,
                                "lineAlpha": 0.4,
                                "title": "Visits",
                                "valueField": "visits",
                                "type": "smoothedLine"
                            }],
                            "plotAreaBorderAlpha": 0,
                            "marginTop": 10,
                            "marginLeft": 0,
                            "marginBottom": 0,
                            "chartCursor": {
                                "cursorAlpha": 0
                            },
                            "categoryField": "date",
                            "categoryAxis": {
                                "startOnAxis": true,
                                "axisColor": "#DADADA",
                                "gridAlpha": 0.07,
                                "title": "Timeline"
                            },
                            "export": {
                                "enabled": false
                            }
                        });
                    } else {
                        this.graphNotifications = 'No data found to render the graph.';
                    }
                },

                // Error callback.
                err => {
                    this.graphNotifications = 'Problem loading the graph data. Please try again after some time.';
                    console.log("Something went wrong!");
                    console.log("Got error in API response");
                    console.log("Error details");
                    console.log(err);
                }
            );
    }

    formatDate(dateValue: string): string {
        let dateObject = new Date(dateValue);
        let options = {
            day: 'numeric',
            month: 'short'
        };
        return dateObject.toLocaleString("en-US", options);
    }
}