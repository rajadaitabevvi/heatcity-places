import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL } from './../config/constants';
import { AnalyticsResponse, AgeRangeData } from './api-response-types/analytics-response';
import { parseMillisecsIntoHoursMinsSecs } from '../lib/helpers';
import { EstablishmentService } from './../services/establishment.service';
import { BizAccountService } from './../services/biz-account.service';

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html'
})

export class DashboardComponent implements OnInit {
    establishmentId: string;

    today: string;

    noOfVisitorsToday: number;

    avgTimeSpentToday: string;

    noOfVisitorsCurrentlyAtVenue: number;
    
    demographicAgeRange: string;

    demographicGender: string;

    establishmentName: string;

    establishmentCapacity: number;

    constructor(
        private http: HttpClient,
        private establishmentService: EstablishmentService,
        private bizAccountService: BizAccountService
    ) {
        this.establishmentId = this.bizAccountService.getEstablishmentId();

        this.setToday();

        /**
         * Set default dashboard statistics data.
         */
        this.noOfVisitorsToday = 0;

        this.avgTimeSpentToday = '';

        this.noOfVisitorsCurrentlyAtVenue = 0;

        this.demographicAgeRange = '';

        this.demographicGender = '';

        this.establishmentName = '';

        // Default establishment capacity is 200.
        // @TODO Get default establishment capacity value from config/settings file.
        this.establishmentCapacity = 200;
    }

    ngOnInit(): void {
        this.getDashboardStats();
        this.loadEstablishmentData();
    }

    // Set time string for today.
    setToday(): void {
        let currentDate = new Date();

        // Set "today"
        // weekday along with long date: Wednesday, October 4, 2017
        let options = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        this.today = currentDate.toLocaleString("en-US", options);
    }

    // Gets dashboard statistics data by calling the API.
    getDashboardStats(): void {
        this.http
            .get<AnalyticsResponse>(`${API_BASE_URL}/api/establishments/${this.establishmentId}/statistics`)
            .subscribe(
                // Success callback.
                data => {
                    console.log("getDashboardStats");
                    console.log(data);

                    // Set dashboard statistics.

                    // Set number of visitors today.
                    this.noOfVisitorsToday = (data.visited && data.visited.today) ? data.visited.today : 0;

                    // Set average time spent today.
                    this.avgTimeSpentToday = (data.time && data.time.average) ? this.formatAvgTimeSpent(data.time.average) : "Not available";

                    // Set no. of visitors currently at venue.
                    this.noOfVisitorsCurrentlyAtVenue = (data.crowd && data.crowd.current) ? data.crowd.current : 0;

                    // Set demographic age range.
                    this.demographicAgeRange = (data.ages !== undefined) ? this.getAgeRangeWithMaxCount(data.ages) : "Age range not available";

                    // Set demographic gender.
                    this.demographicGender = (data.gender !== undefined) ? this.getDemographicGender(data.gender) : "Gender not available";
                },

                // Error callback.
                err => {
                    console.log("Something went wrong!");
                    console.log("Got error in API response");
                    console.log("Error details");
                    console.log(err);
                }
            );
    }

    // Formats average time spent data into human readable format.
    // x hr y min
    formatAvgTimeSpent(milliseconds): string {
        let timeData = parseMillisecsIntoHoursMinsSecs(milliseconds);
        return `${timeData[0]} hr ${timeData[1]} min`;
    }

    // Gets AgeRangeData object with maximum count.
    getAgeRangeWithMaxCount(ages: AgeRangeData[]): string {
        let maxIndex = 0;
        let maxCount = 0;

        for (let i = 0; i < ages.length; i++) {
            if (ages[i].ageCount > maxCount) {
                maxCount = ages[i].ageCount;
                maxIndex = i;
            }
        }

        // Check if there are 0 counts in all then return a message.
        return (maxCount > 0) ? ages[maxIndex].age : 'Age range not available';
    }

    getDemographicGender(genderData): string {
        let gender = '';

        // If genderData is defined
        if (genderData !== undefined) {
            // If both male and female are defined
            if ((genderData.male !== undefined) && (genderData.female !== undefined)) {
                // If both are unequal then select one with max value.
                if (genderData.male !== genderData.female) {
                    gender = (genderData.male > genderData.female) ? 'Male' : 'Female';
                }
            }
            // Else if male is defined
            else if (genderData.male !== undefined) {
                gender = (genderData.male > 0) ? 'Male' : '';
            }
            // Else if female is defined
            else if (genderData.female !== undefined) {
                gender = (genderData.female > 0) ? 'Female' : '';
            }
        }

        return gender;
    }

    loadEstablishmentData(): void {
        this.establishmentService
            .getEstablishment(this.establishmentId)
            .subscribe(
                data => {
                    this.establishmentName = data.name;
                    this.establishmentCapacity = (data.capacity === undefined) ? 200 : data.capacity;
                },
                error => {
                    console.log('Error while getting Establishment data');
                    console.log(error);
                }
            );
    }    
}