export class PromotionType {
    constructor (
        public name: string = '',
        public description: string = '',
        public amount: number = 0,
        public startsAt: string = '',
        public endsAt: string = '',
        public areaName: string = '',
        public promoRadius: number = 0,
        public ageRange: string = '',
        public targeted: boolean = false,
        public establishmentId: string = '',
        public startDate: string = '',
        public startTime: string = '',
        public endDate: string = '',
        public endTime: string = ''
    ) {}

    reset(): void {
        this.name = '';
        this.description = '';
        this.amount = 0;
        this.startsAt = '';
        this.endsAt = '';
        this.areaName = '';
        this.promoRadius = 0;
        this.ageRange = '';
        this.targeted = false;
        this.establishmentId = '';
        this.startDate = '';
        this.startTime = '';
        this.endDate = '';
        this.endTime = '';        
    }
}