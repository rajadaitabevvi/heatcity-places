export class DateRange {
    constructor(
        public startDate: string = '',
        public endDate: string = ''
    ) {}
}