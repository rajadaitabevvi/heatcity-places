export class AgeRangePieChartData {
    constructor(
        public age: string = '',
        public ageCount: number = 0
    ) {}
}