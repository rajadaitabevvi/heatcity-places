import { AmChart } from '@amcharts/amcharts3-angular';

export class File {
    constructor(
        public filename: string = "",
        public path: string = ""
    ) {}
}

export class Photo {
    constructor(
        public file: File = new File(),
        public id: string = "",
        public establishmentId: string = "",
        public accountId: string = "",
        public createdAt: string = "",
        public updatedAt: string = ""
    ) {}
}

export class EstablishmentCompareData {
    constructor(
        public competitorEstablishmentId: string = '',
        public competitorEstablishmentName: string = '',
        public competitorEstablishmentVisitsCount: number = 0,
        public ownerEstablishmentId: string = '',
        public ownerEstablishmentName: string = '',
        public ownerEstablishmentVisitsCount: number = 0,
        public comparisonStats: string = '',
        public graphNotifications: string = '',
        public graphReady: boolean = false,
        public amChart: AmChart = null,
        public photos: Photo[] = []
    ) {}
}

export interface EstablishmentCompareDataArray {
    [key: string]: EstablishmentCompareData;
}