interface EstablishmentId {
    establishmentId: string;
}

export class BizAccount {
    constructor(
        public id: string = "",
        public code: string = "",
        public phoneNumber: string = "",
        public approved: boolean = false,
        public establishmentIds: EstablishmentId[] = [],
        public password: string = "",
        public email: string = "",
        public hcActive: number = 0
    ) {}

    reset(): void {
        this.id = "";
        this.code = "";
        this.phoneNumber = "";
        this.approved = false;
        this.establishmentIds = [];
        this.password = "";
        this.email = "";
        this.hcActive = 0;
    }
}