export const API_BASE_URL = 'http://dev.heat.city';
export const IMG_BASE_URL = 'http://api.heat.city/v1';

export const MONTHS: string[] = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];