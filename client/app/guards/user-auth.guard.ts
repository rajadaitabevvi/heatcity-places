import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './../services/auth.service';

@Injectable()
export class UserAuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        // If logged in then return true.
        if (this.authService.isBizAccountLoggedIn()) {
            return true;
        }

        // not logged in so navigate user to the Home page.
        this.router.navigate(['/']);

        return false;
    }
}