import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './../services/auth.service';

@Injectable()
export class AdminAuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        // If admin user logged in then return true.
        if (this.authService.isAdminLoggedIn()) {
            return true;
        }

        // not logged in so navigate user to the admin login page.
        this.router.navigate(['/admin/login']);

        return false;
    }
}