interface EstablishmentId {
    establishmentId: string;
}

export interface BizAccountResponse {
    id: string;
    approved: boolean;
    code: string;
    email: string;
    phoneNumber: string;
    establishmentIds: EstablishmentId[];
    hcActive: number;
}