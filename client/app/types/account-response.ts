export interface AccountResponse {
    code: string;
    phoneNumber: string;
    approved: boolean;
    email: string;
    id: string;
    createdAt: string;
    updatedAt: string;
}