export class Account {
    constructor(
        public id: string = "",
        public email: string = "",
        public phoneNumber: string = "",
        public approved: boolean = true,
        public password: string = ""
    ) {}

    reset(): void {
        this.id = "";
        this.email = "";
        this.phoneNumber = "";
        this.approved = true;
        this.password = "";
    }
}