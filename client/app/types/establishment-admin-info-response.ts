export interface EstablishmentAdminInfoResponse {
    id: string;
    firstName: string;
    lastName: string;
    middleName: string;
    displayName: string;
    gender: string;
    hometown: string;
    homecountry: string;
    code: string;
    phoneNumber: string;
    approved: boolean;
    bio: string;
    realm: string;
    username: string;
    email: string;
    emailVerified: boolean;
    status: string;
    hcActive: number;
}