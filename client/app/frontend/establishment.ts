export class Establishment {
    constructor(
        public name: String = "",
        public address: String = ""
    ) {}

    reset(): void {
        this.name = "";
        this.address = "";
    }
}