export class BusinessAccount {
    constructor(
        public email: string = "",
        public phoneNumber: string = "",
        public agreeServiceTerms: boolean = false,
        public infoAccurate: boolean = false
    ) {}

    reset(): void {
        this.email = "";
        this.phoneNumber = "";
        this.agreeServiceTerms = false;
        this.infoAccurate = false;
    }
}