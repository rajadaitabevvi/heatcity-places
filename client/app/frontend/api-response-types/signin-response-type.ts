export interface SigninResponseType {
    id: string;
    ttl: number;
    userId: string;
}