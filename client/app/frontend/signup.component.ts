import { Component } from '@angular/core';
import { Establishment } from './establishment';
import { BusinessAccount } from './business-account';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'signup',
    templateUrl: './signup.component.html'
})

export class SignupComponent {
    establishment: Establishment;

    businessAccount: BusinessAccount;

    // Current establishments in the system.
    establishments: Establishment[];

    // To show different parts of the signup form.
    // Step-1 = Establishment Verification.
    // Step-2 = Business Account Sign-up.
    // Step-3 = Sign-up Notification.
    step: number;

    signupErrorMsg: string;

    // Inject HttpClient.
    constructor(
        private http: HttpClient
    ) {
        this.establishment = new Establishment();
        this.businessAccount = new BusinessAccount();
        this.signupErrorMsg = "";
        this.step = 1;
    }

    ngOnInit(): void {
        this.loadEstablishments();
    }

    loadEstablishments(): void {
        // Make the http request to get establishments.
        this.http
            .get('/api/establishments')
            .subscribe(
                // Successful responses call the first callback.
                data => {
                    this.establishments = data['establishments']
                },

                // Errors will call this callback instead.
                err => {
                    console.log("Something went wrong!");
                    console.log(err);
                }
            )
    }

    verifyEstablishment(): void {
        console.log('called verifyEstablishment');
        // Set POST data to be submitted in API.
        let body = {
            establishmentName: this.establishment.name,
        }

        // Move to Step-2.
        this.step = 2;
    }

    doSignup(signupForm: NgForm): void {
        // Set POST data to be submitted in API.
        let body = {
            establishmentName: this.establishment.name,
            establishmentAddress: this.establishment.address,
            businessAccountEmail: this.businessAccount.email,
            businessAccountPhoneNumber: this.businessAccount.phoneNumber
        }

        // Reset signup form.
        signupForm.resetForm();

        // Make the HTTP request to Signup API.
        this.http
        .post('/api/business-account/signup', body)
        .subscribe(
            // Successful responses call the data callback.
            data => {
                // Move to Step-3.
                this.step = 3;
            },

            // Errors will call this callback instead.
            err => {
                if (err.status == 500) {
                    this.signupErrorMsg = err.error.message;
                } else {
                    let errorObject = JSON.parse(err.error);
                    this.signupErrorMsg = errorObject.message;
                }
            }
        )
    }

    backToSignup(): void {
        this.establishment.reset();
        this.businessAccount.reset();
        this.signupErrorMsg = "";
        this.step = 1;
    }
}