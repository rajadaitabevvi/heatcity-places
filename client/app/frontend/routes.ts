import { Routes } from '@angular/router';

import { HomeComponent } from './home.component';

export const FRONTEND_ROUTES: Routes = [
    {
        path: '',
        component: HomeComponent
    }
];