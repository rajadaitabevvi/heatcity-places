import { Component } from '@angular/core';

@Component({
  selector: 'home',
  templateUrl: './home.component.html'
})

export class HomeComponent {
    title: string = 'Home';

    constructor() {
        console.log("user data in local storage");
        console.log(localStorage.getItem("currentUser"));
    }
}