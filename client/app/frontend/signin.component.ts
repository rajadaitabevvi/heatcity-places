import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';
import { BizAccountService } from './../services/biz-account.service';
import { EstablishmentService } from './../services/establishment.service';
import { BizAccountResponse } from './../types/biz-account-response';
declare var jquery:any;
declare var $ :any;

@Component({
    selector: 'signin',
    templateUrl: './signin.component.html',
    providers: [
        AuthService,
        BizAccountService
    ]
})

export class SigninComponent {
    loginModel: any;

    signinError: string;

    constructor(
        private authService: AuthService,
        private bizAccountService: BizAccountService,
        private establishmentService: EstablishmentService,
        private router: Router
    ) {
        this.loginModel = {};
        this.signinError = "";
    }

    doSignin(): void {
        this.authService
        .bizAccountLogin(this.loginModel.email, this.loginModel.password)
        .subscribe(
            data => {
                // Clear the sign in error.
                this.signinError = "";

                // Save user data in the local storage.
                localStorage.setItem('currentUser', JSON.stringify(data));

                let bizAccountId = this.bizAccountService.getBizAccountId();

                // Get establishment id of the biz account and save it in the local storage.
                this.bizAccountService.findUserById(bizAccountId)
                .subscribe(
                    data => {
                        let bizAccountResponse: BizAccountResponse = data;

                        // Set establishmentId into local storage.
                        localStorage.setItem("establishmentId", bizAccountResponse.establishmentIds[0].establishmentId);

                        // Redirect to the /account/dashboard page.
                        this.router.navigate(['/account/dashboard']);
                    },
                    error => {
                        console.log("Failed to get business account data");
                        console.log(error);
                    }
                );

                // Hide the sign in pop up overlay.
                $('#signin').modal('hide');
            },
            error => {
                console.log('Failed to login to business account');
                this.signinError = "Failed to signin. Please provide correct email and password.";
                console.log(error);
            }
        );
    }
}