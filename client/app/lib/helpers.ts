/**
 * A library of common helper functions.
 */

// A function to get Hours, Minutes, Seconds from Milliseconds.
// Parameters: milliseconds: number
// Returns:    An array containing Hours, Minutes, Seconds.
//    [
//        15 (hours),
//        50 (minutes),
//        45 (seconds)
//    ]
export function parseMillisecsIntoHoursMinsSecs(milliseconds: number): number[] {
    // Get hours from milliseconds.
    let hours = milliseconds / (1000 * 60 * 60);
    let absoluteHours = Math.floor(hours);

    // Get remainder from hours and convert to minutes.
    let minutes = (hours - absoluteHours) * 60;
    let absoluteMinutes = Math.floor(minutes);

    // Get remainder from minutes and convert to seconds.
    let seconds = (minutes - absoluteMinutes) * 60;
    let absoluteSeconds = Math.floor(seconds);

    return [ absoluteHours, absoluteMinutes, absoluteSeconds ];
}

export function parseMillisecsIntoMins(milliseconds: number): number {
    // Get minutes from milliseconds.
    let mins = milliseconds / (1000 * 60);
    return Math.floor(mins);
}
