jQuery(document).ready(function () {
    // jQuery code for smooth scrolling.
    jQuery('.smooth-scroll').on('click', function (event) {
        var target = jQuery(this.getAttribute('href'));
        if (target.length) {
            event.preventDefault();
            jQuery('html, body').stop().animate({
                scrollTop: target.offset().top
            }, 1000);
        }
    });

    // Menu Toggle Script.
    // Since the elements are added dynamically in the DOM use the following way to bind the events.
    jQuery(document).on('click', '#menu-toggle', function(event) {
        event.preventDefault();
        jQuery("#wrapper").toggleClass("toggled");
    });

    // Disable linking behaviour.
    jQuery(".no-link").click(function (e) {
        e.preventDefault();
    });

    bs_input_file();
});

function bs_input_file() {
    jQuery(".input-file").before(
        function () {
            if (!jQuery(this).prev().hasClass('input-ghost')) {
                var element = jQuery("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                element.attr("name", jQuery(this).attr("name"));
                element.change(function () {
                    element.next(element).find('input').val((element.val()).split('\\').pop());
                });
                jQuery(this).find("button.btn-choose").click(function () {
                    element.click();
                });
                jQuery(this).find("button.btn-reset").click(function () {
                    element.val(null);
                    jQuery(this).parents(".input-file").find('input').val('');
                });
                jQuery(this).find('input').css("cursor", "pointer");
                jQuery(this).find('input').mousedown(function () {
                    jQuery(this).parents('.input-file').prev().click();
                    return false;
                });
                return element;
            }
        }
    );
}
