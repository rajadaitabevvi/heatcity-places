// Get dependencies.
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
var mongoose = require('mongoose');


// Define our app using Express.
const app = express();


// Configure app to use body-parser.
// This will let us get the data from a POST.
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


// Connect to the MongoDB database.
mongoose.connect('mongodb://127.0.0.1:27017/blue-dragon-dev', {
    useMongoClient: true,
    promiseLibrary: global.Promise
});
const db = mongoose.connection;
// Handle database connection error.
db.on('error', console.error.bind(console, 'DB connection error:'));
// Database connection successful.
db.once('open', function () {
    console.log('Connected to the DB successfully.');
});


// Get API routes.
const apiRoutes = require('./routes/api');
// Set API routes.
app.use('/api', apiRoutes);
// Point static path to dist.
app.use(express.static(path.join(__dirname, '../dist')));
// Catch all other routes and return the index file.
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../dist/index.html'));
});


// Start application server.
// Get port from environment and store in Express.
const port = process.env.PORT || 3038;
app.listen(port);
console.log(`Express REST API Server is started on http://localhost:${port}`);
