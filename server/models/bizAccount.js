const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var BizAccountSchema = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    emailVerified: Boolean,
    phoneNumber: String,
    approved: Boolean,
    password: String,
    establishmentId: String,
    last_login: Date,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
}, {
    collection: 'BizAccount'
});

module.exports = mongoose.model('BizAccount', BizAccountSchema);
