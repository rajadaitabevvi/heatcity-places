const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var EstablishmentSchema = new Schema({
    name: String,
    address: String,
    capacity: Number,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
}, {
    collection: 'Establishment'
});

module.exports = mongoose.model('Establishment', EstablishmentSchema);
